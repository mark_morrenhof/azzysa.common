package infostrait.azzysa.ev6.settings;

import java.util.AbstractMap;
import matrix.db.Context;
import matrix.db.Page;
import matrix.db.PageList;

/**
 * Settings provider for the azzysa platform
 * @author m.morrenhof
 */
public class Provider {
    
    public static final String GLOBAL_CONFIG_NAME = "azzysa.settings";
    private Settings globalSettings;
    private Settings siteSettings;
    private Settings machineSettings;
    private Settings effectiveSettings;
    private String machineName;
    private String siteName;
    
    /**
     * Default constructor
     */
    public Provider() {
        // Default constructor
        this.globalSettings = new Settings();
        this.machineSettings = new Settings();
        this.siteSettings = new Settings();
        this.effectiveSettings = new  Settings();
    }
    
    /**
     * Initialize the provider, read settings from the system
     * @param context Active and connected Matrix Context who has access to read the page configuration objects
     * @param site Name of the site to check its site specific configuration object for
     * @param machine Name of the machine to check its specific configuration object for
     * @return Effective configuration settings for the site and machine combination
     * @throws Exception 
     */
    public Settings Initialize(Context context, String site, String machine) throws Exception {
        Page globalSettingsPage;
        
        // Store the site and machine name
        siteName = site;
        machineName = machine;
        if (siteName == null) {
            siteName = "";
        } else {
            siteName = siteName.toLowerCase();
        }
        if (machineName == null) {
            machineName = "";
        } else {
            machineName = machineName.toLowerCase();
        }
                        
        // Get the page object, and check if it exists
        globalSettingsPage = new Page(GLOBAL_CONFIG_NAME);
        if (globalSettingsPage.exists(context)) {
            globalSettingsPage.open(context);
            globalSettings = Settings.parse(globalSettingsPage.getContents(context));
            globalSettingsPage.close(context);
        } else {
            // Does not exists
            throw new Exception("Unable to get the global configuration by name: " + GLOBAL_CONFIG_NAME);
        }

        // Generate configuration name for the site and machine configuration pages
        String siteConfigName = GLOBAL_CONFIG_NAME + "." + site;
        String machineConfigName = siteConfigName + "." + machine;
                
        // Get site/machine configuration as well (if exists)
        PageList pages = Page.getPages(context);
        for (int i = 0; i < pages.size() - 1; i++) {
            ((Page) pages.get(i)).open(context);
            
            if (siteConfigName.equalsIgnoreCase(((Page) pages.get(i)).getName(context))) {
                // Site configuration
                siteSettings = Settings.parse(((Page) pages.get(i)).getContents(context));
            } 
            else if (machineConfigName.equalsIgnoreCase(((Page) pages.get(i)).getName(context))) {
                // Machine configuration
                machineSettings = Settings.parse(((Page) pages.get(i)).getContents(context));
            }
            ((Page) pages.get(i)).close(context);
        }
        
        // Determine the effective settings defined as system level
        for (Setting setting : globalSettings.Settings) {
            AbstractMap.SimpleEntry<Boolean, String> entry;
            String effectiveValue;
            
            // By default, set the effective value to the value of the default setting
            effectiveValue = setting.Value;
            
            // Check if setting exists in site configuration
            entry = siteSettings.CheckEntry(setting.Key);
            if (entry.getKey()) {
                effectiveValue = entry.getValue();
            }
            
            // Check if setting exists in machine configuration
            entry = machineSettings.CheckEntry(setting.Key);
            if (entry.getKey()) {
                effectiveValue = entry.getValue();
            }
            
            // Add the effective value to the effective settings collection
            effectiveSettings.Settings.add(new Setting(setting.Key, effectiveValue));
        }
        
        // Determine settings that are defined on site level but not yet in effective settings collection
        // And therefore, these settings are not defined in the system scope
        for (Setting setting : siteSettings.Settings) {
            AbstractMap.SimpleEntry<Boolean, String> entry;
            String effectiveValue;
            
            // Check if setting already exists in the effective setting collection 
            if (!effectiveSettings.CheckEntry(setting.Key).getKey()) {
                // Does not yet exists
                // By default, set the effective value to the value of the site setting
                effectiveValue = setting.Value;

                // Check if setting exists in machine configuration
                entry = machineSettings.CheckEntry(setting.Key);
                if (entry.getKey()) {
                    effectiveValue = entry.getValue();
                }

                // Add the effective value to the effective settings collection
                effectiveSettings.Settings.add(new Setting(setting.Key, effectiveValue));
                
            }
        }
        
        // Determine settings that are defined on machine level but not yet in effective settings collection
        // And therefore, these settings are not defined in the system or site scope
        for (Setting setting : machineSettings.Settings) {
            AbstractMap.SimpleEntry<Boolean, String> entry;
            String effectiveValue;
            
            // Check if setting already exists in the effective setting collection 
            if (!effectiveSettings.CheckEntry(setting.Key).getKey()) {
                // Does not yet exists
                // By default, set the effective value to the value of the machine setting
                effectiveValue = setting.Value;
                
                // Add the effective value to the effective settings collection
                effectiveSettings.Settings.add(new Setting(setting.Key, effectiveValue));
                
            }
        }
                
        // Return the effective settings set
        return effectiveSettings;
            
    }
    
    /**
     * Return the effective settings for the site and machine
     * @return 
     */
    public Settings getSettings() {
        return effectiveSettings;
    }
    
    /**
     * Returns a new instance of the setting provider
     * @return 
     */
    public static Provider getInstance() {
        return new Provider();
    }
    
}