package infostrait.azzysa.ev6.settings;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.AbstractMap;
import java.util.ArrayList;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Settings collection class
 * @author m.morrenhof
 */
@XmlRootElement(name="Settings")
public class Settings {
    
    @XmlElement(required = true, name="Setting")
    public ArrayList<Setting> Settings;
    
    public Settings() {
        this.Settings = new ArrayList<Setting>();
    }
    
    /**
     * Internal helper method the check whether a setting by name exists and if so, return its value
     * @param name
     * @return 
     */
    public AbstractMap.SimpleEntry<Boolean, String> CheckEntry(String name) {
        for (Setting setting : Settings) {
            if (setting.Key.equalsIgnoreCase(name)) {
                return new AbstractMap.SimpleEntry<Boolean, String>(Boolean.TRUE, setting.Value);
            }
        }
        
        // Not found, does not exists
        return new AbstractMap.SimpleEntry<Boolean, String>(Boolean.FALSE, "");
        
    }
      
    /**
     * Serializes this settings collection as a XML string
     * @return
     * @throws JAXBException 
     */
    public String settingsAsString() throws JAXBException {
        JAXBContext __Ctx;
        Marshaller __M;
        StringWriter __W;
        
        // Initialize serializer
        __Ctx = JAXBContext.newInstance(Settings.class);
        __M = __Ctx.createMarshaller();
        __M.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        __W = new StringWriter();
        
        // Serialize
        __M.marshal(this, __W);
        
        // Return results
        return __W.toString();       
    }
    
    /**
     * Reads as string serialized settings collection into an in-memory settings collection object
     * @param xmlString
     * @return  De-serialized package
     */
    public static Settings parse(String xmlString) throws JAXBException {
        Settings _settings;
        JAXBContext _Ctx;            
        Unmarshaller _Un;
        StringReader _Rdr;
                                
        // Check paramter
        _settings = new Settings();
        if ("".equalsIgnoreCase(xmlString)) {
            // Empty package string, return new data package
        } else {
            // Translate package string into in-memory package object
            
            // Use JAXB implementation to de-serialize the XML string
            _Ctx = JAXBContext.newInstance(Settings.class);
            _Un = _Ctx.createUnmarshaller();
            _Rdr = new StringReader(xmlString);
            
            // De-serialize
            _settings = (Settings)_Un.unmarshal(_Rdr);
            _Rdr.close();
        }

        // Finished
        return _settings;
    }
}