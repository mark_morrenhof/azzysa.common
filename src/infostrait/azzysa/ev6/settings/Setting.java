/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package infostrait.azzysa.ev6.settings;

/**
 *
 * @author m.morrenhof
 */
public class Setting {
    
    public String Key;
    public String Value;
    
    
    public Setting() {
        this.Key = "";
        this.Value = "";
    }
    
    public Setting(String key, String value) {
        this.Key = key;
        this.Value = value;
    }
    
    @Override
    public String toString() {
        return Key + " [" + Value + "]";
    }
    
}
