package infostrait.azzysa.ev6.commonobjects;

/**
 * Collection of data objects
 * @author m.morrenhof
 */
public class DataObjects {
    
    /**
     * Collection of data objects
     * @see ObjectCollection
     */
    public ObjectCollection ObjectCollection;
    
    /**
     * Constructs a new object collection
     */
    public DataObjects() {
        this.ObjectCollection = new ObjectCollection();
    }
    
}