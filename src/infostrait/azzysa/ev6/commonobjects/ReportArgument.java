package infostrait.azzysa.ev6.commonobjects;

/**
 * Input argument for a report
 * @author t.v.oost
 */
public class ReportArgument {
    public String Name;
    public Object Value;
}
