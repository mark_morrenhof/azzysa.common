package infostrait.azzysa.ev6.commonobjects;

import matrix.db.Context;

/**
 * Internal class used by the DIADataService
 * @author m.morrenhof
 */
public class MethodInvocationSet {
 
    /**
     * Holds the MethodInfo of the method to invoke
     */
    public java.lang.reflect.Method Method;
    
    /**
     * Holds the ConstructorInfo to use when instantiating an object
     */
    public java.lang.reflect.Constructor Constructor;
    
    /**
     * Object array holding the parameter values used for the invocation
     */
    public Object[] InvokeParameters;
    
    /**
     * Function constructs the method object to invoke including processing any function result parameters
     * @param context
     * @param classLoader
     * @param dataObject
     * @param methodClass
     * @param methodParams
     * @param methodName
     * @param IsConstructor
     * @return
     * @throws Exception 
     */
    public static MethodInvocationSet getMethodInvocation(Context context, ClassLoader classLoader, DataObject dataObject, Class methodClass, Parameters methodParams, String methodName, boolean IsConstructor) throws Exception {
        Class _MethodParams[];        
        int idx;
        String sAttributeName;        
        MethodInvocationSet _Result;
        Object invocationResult;
                
        // Collect parameters
        _Result = new MethodInvocationSet();
        _MethodParams = new Class[methodParams.Parameter.size()];

        _Result.InvokeParameters = new Object[methodParams.Parameter.size()];
        idx = 0;
        for (Parameter param : methodParams.GetParametersSorted()) {
            _MethodParams[idx] = classLoader.loadClass(param.Type);

            if (param.Type.equals(Context.class.getName())) {
                // Matrix Context object requested
                _Result.InvokeParameters[idx] = context;

            } else if (("".equals(param.Value) || param.Value == null) && param.FunctionResult != null) {
                // Use function result
                // Invoke method
                invocationResult = param.FunctionResult.DoCollectFunctionResult(context, dataObject, classLoader);

                // Set result
                _Result.InvokeParameters[idx] = invocationResult;

            } else if (("".equals(param.Value) || param.Value == null) && param.invocation != null) {
                // Use type invocation result
                // Invoke method, since the DoCollectInvocationResult operation always puts the object to instantiate
                // at the first position in the array, use this item as the invocation result
                invocationResult = param.invocation.DoCollectInvocationResult(context, dataObject, classLoader).get(0);

                // Set result
                _Result.InvokeParameters[idx] = invocationResult;

            } else if (param.Value.startsWith("${ATTRIBUTES") && param.Value.endsWith("}")) {
                // Create matrix.db.AttributeList
                _Result.InvokeParameters[idx] = Utility.ToAttributeList(context, dataObject, param.Value);

            } else if (param.Value.startsWith("$DATA_OBJECT")) {
                // Set the entire data object as a parameter
                _Result.InvokeParameters[idx] = dataObject;

            } else if (param.Value.startsWith("{") && param.Value.endsWith("}")) {
                // Use object value
                sAttributeName = param.Value.substring(1);
                sAttributeName = sAttributeName.substring(0, sAttributeName.length() - 1);

                // Check whether attribute exists
                if (dataObject.Attributes.ContainsAttribute(sAttributeName)) {
                    // Attribute exists, use its value
                    _Result.InvokeParameters[idx] = dataObject.Attributes.GetValueAsString(sAttributeName);
                } else {
                    // Attribute does not exists, throw new exception
                }

            } else {
                if ("${NULL}".equalsIgnoreCase(param.Value)) {
                    // Set Null Value
                    _Result.InvokeParameters[idx] = null;

                } else {
                    // Use literal text
                    _Result.InvokeParameters[idx] = param.Value;
                }
            }

            idx = idx + 1;
        }

        if (IsConstructor) {
            _Result.Constructor = methodClass.getConstructor(_MethodParams);
        } else {
            _Result.Method = methodClass.getMethod(methodName, _MethodParams);
        }

        // Finished     
        return _Result;
    }
}