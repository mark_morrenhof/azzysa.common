package infostrait.azzysa.ev6.commonobjects;

/**
 * Represents an attribute holding name and value of a single attribute
 * @author m.morrenhof
 */
public class ObjectAttribute {
    
    /**
     * Name of the attribute
     */
    public String Name;
    
    /**
     * Value of the attribute
     */
    public String Value;
    
    public ObjectAttribute() {
        
    }
    
    public ObjectAttribute(String name, String value) {
        this.Name = name;
        this.Value = value;
    }
 
    public String toString() {
    	return this.Name + "[" + this.Value + "]";
    }
    
}