package infostrait.azzysa.ev6.commonobjects;

import javax.xml.bind.annotation.XmlElement;
import matrix.db.AttributeList;
import matrix.db.Context;
import matrix.db.MatrixLogWriter;

/**
 * Specifies a function to invoke
 * @author m.morrenhof
 */
public class FunctionResult {

    /**
     * Invocation style: Static / Instance
     */
    @XmlElement(required = true, name="InvocationModifier")
    public String InvocationModifier;
    
    /**
     * Name of function to invoke
     */
    @XmlElement(required = true, name="FunctionName")
    public String FunctionName;

    /**
     * Name of the Java Type which exposes the function to invoke
     */
    @XmlElement(required = true, name="TypeName")
    public String TypeName;

    /**
     * Parameter collection required to invoke the method
     * @see Parameters
     */
    @XmlElement(required = true, name="Parameters")
    public Parameters Parameters;
    
    /**
     * Parameter collection required to instantiate this function type
     */
    @XmlElement(required = true, name="ConstructorParameters")
    public Parameters ConstructorArgs;
        
    /**
     * Constructs a new empty function result object
     */
    public FunctionResult(){
        this.Parameters = new Parameters();
        this.ConstructorArgs = new Parameters();
        this.InvocationModifier = "Static";
    }

    /**
     * Function invokes the requested function as specified in functionResult and returns the result as an object
     * @param context Connected ENOVIA context
     * @param dataObject DataObject object holding all the source attributes
     * @param classLoader Class loader that is used to load the requested types by this function result instruction
     * @return The result of the function invocation
     * @throws Exception 
     */
    public Object DoCollectFunctionResult(Context context, DataObject dataObject, ClassLoader classLoader) throws Exception {
        Class _Class;
        Object _Object;
        MethodInvocationSet _Method;
        MethodInvocationSet _Constructor;
        Object _Result;
        matrix.util.StringList errorMessages;   
        int idx;
        
        // Log for troubleshoot/debug purposes            
        errorMessages = new matrix.util.StringList();
        errorMessages.addElement("[DoCollectFunctionResult] : Method name: " + FunctionName);

        // Get class to load
        _Class = classLoader.loadClass(TypeName);
        errorMessages.addElement("[DoCollectFunctionResult] : Java Type found: " + TypeName);

        // Get method to invoke
        _Method = MethodInvocationSet.getMethodInvocation(context, classLoader, dataObject, _Class, Parameters, FunctionName, false);
        errorMessages.addElement("[DoCollectFunctionResult] : Method invocation retrieved...");

        // Log
        idx = 0;
        for (Object invokeParam : _Method.InvokeParameters) {
            if (invokeParam == null) {
                errorMessages.addElement("[DoCollectFunctionResult] : Type [" + 
                        Parameters.GetParametersSorted().get(idx).Type + "], Value [NULL]");
            } else {
                if (invokeParam.getClass().toString().equalsIgnoreCase(matrix.db.AttributeList.class.toString())) {
                    AttributeList list;
                    list = (AttributeList)invokeParam;
                    for (int iAttr = 0; iAttr < list.size(); iAttr++) {
                        errorMessages.addElement("[DoCollectFunctionResult] : Type [" + 
                                invokeParam.getClass().toString() + "], Value [" + 
                                iAttr + " - " + ((matrix.db.Attribute)list.get(iAttr)).getName() + 
                                "]: " + ((matrix.db.Attribute)list.get(iAttr)).getValue());
                    }
                } else {
                    errorMessages.addElement("[DoCollectFunctionResult] : Type [" + 
                            invokeParam.getClass().toString() + "], Value [" + 
                            invokeParam + "]");
                }
            }
            idx++;
        }

        // Check invocation type (do not instantiate abstract classes)
        if ("static".equalsIgnoreCase(InvocationModifier)) {
            // Do not instantiate, do not instantiate object
            errorMessages.addElement("[DoCollectFunctionResult] : Method invocation type set to static...");
            _Object = null;
        } else {
            // Instantiate object     
            if (ConstructorArgs.Parameter.size() > 0) {
                // Use parameterized constructor
                errorMessages.addElement("[DoCollectFunctionResult] : Use parameterized constructor...");

                // Get method invocation set for constructor
                _Constructor = MethodInvocationSet.getMethodInvocation(context, classLoader, dataObject, _Class, ConstructorArgs, "__Constructor", true);
                errorMessages.addElement("[DoCollectFunctionResult] : Constructor information retrieved...");

                // Instantiate object
                _Object = _Constructor.Constructor.newInstance(_Constructor.InvokeParameters);

            } else {
                // Use parameterless constructor
                errorMessages.addElement("[DoCollectFunctionResult] : Use parameterless constructor...");
                _Object = _Class.newInstance();
            }
            errorMessages.addElement("[DoCollectFunctionResult] : Type instantiated...");
        }

        // Invoke method
        errorMessages.addElement("[DoCollectFunctionResult] : About to perform method invocation...");
        _Result = _Method.Method.invoke(_Object, _Method.InvokeParameters);
        errorMessages.addElement("[DoCollectFunctionResult] : Method invoked, return value type " + 
                _Result.getClass().getName());

        // Finished
        return _Result;
            
    }
}