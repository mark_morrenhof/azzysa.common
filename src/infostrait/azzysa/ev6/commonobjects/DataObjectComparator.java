/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package infostrait.azzysa.ev6.commonobjects;

import java.util.Comparator;

/**
 *
 * @author m.morrenhof
 */
public class DataObjectComparator implements Comparator<DataObject> {
    
    private String _AttributeToCompare;
    
    public DataObjectComparator(String attributeToCompare) {
        _AttributeToCompare = attributeToCompare;
    }
    
     /**
     * Internal use only, compare type instructions
     * @param o1
     * @param o2
     * @return 
     */
    @Override
    public int compare(DataObject o1, DataObject o2) {
        return o1.Attributes.GetValueAsStringWithoutNullOrException(_AttributeToCompare).compareTo(o1.Attributes.GetValueAsStringWithoutNullOrException(_AttributeToCompare));
    }
    
}