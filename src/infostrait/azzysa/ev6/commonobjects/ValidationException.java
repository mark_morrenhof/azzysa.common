/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package infostrait.azzysa.ev6.commonobjects;

/**
 *
 * @author m.morrenhof
 */
public class ValidationException extends Exception {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -8292379149555055078L;

	public ValidationException(String message) {
        super(message);
    }
    
}
