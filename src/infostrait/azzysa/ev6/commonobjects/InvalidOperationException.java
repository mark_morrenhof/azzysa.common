package infostrait.azzysa.ev6.commonobjects;

import java.util.LinkedList;

/**
 * Exception caused by the data object processed mappings, containing the process log
 * @author m.morrenhof
 */
public class InvalidOperationException extends Exception {
    
    private LinkedList<String> _ProcessLog;
    
    public LinkedList<String> GetProcessLog() {
        return _ProcessLog;
    }
    
    public InvalidOperationException(Throwable cause, String message, LinkedList<String> ProcessLog) {
        super(message, cause);
        
        // Store process log
        this._ProcessLog = ProcessLog;
    }
    
}
