package infostrait.azzysa.ev6.commonobjects;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import matrix.db.Context;

/**
 * Represents data object holding collection of object attributes
 * 
 * @author m.morrenhof
 */
public class DataObject {

	/**
	 * Collection of object attributes holding the actual data
	 * 
	 * @see Attributes
	 */
	public Attributes Attributes;

	/**
	 * ObjectId of object that is being processed
	 * 
	 * @see Attributes
	 */
	public String ObjectId;

	/**
	 * Constructs new data object
	 */
	public DataObject() {
		this.Attributes = new Attributes();
	}

	/**
	 * Function that actually imports the data object, using the mapping
	 * 
	 * @param context
	 *            Matrix Context (connected)
	 * @param mapping
	 *            Contains the data model mapping, required to retrieve type
	 *            instructions
	 * @param classLoader
	 * @return Exception text if any, when successfully an empty string
	 * @throws java.lang.Exception
	 */
	public ProcessResult ProcessDataObject(Context context, Mapping mapping, ClassLoader classLoader) throws Exception {
		int iValidMappings = 0;
		ProcessResult result = new ProcessResult();

		try {
			try {
				// Iterate through all entity mappings in the mapping object
				// For each mapping, the expression is checked to verify whether
				// to apply this mapping on the current data object
				for (EntityMapping entityMapping : mapping.EntityMapping) {

					// Check expression
					if (entityMapping.IsValid(this)) {
						// Log
						iValidMappings += 1;

						// Is valid, continue processing by iterating through
						// the invocation collection
						int iTypeInvocation = 1;
						for (TypeInvocation invocation : entityMapping.invocationCollection.invocations) {
							// Invoke the type invocation
							ArrayList<Object> invocationResult;
							invocationResult = invocation.DoCollectInvocationResult(context, this, classLoader);

							// Process the result
							for (Object returnValue : invocationResult) {
								if (returnValue != null) {
									// Append the 'toString' representation of
									// the object
									result.ProcessLog.add("Mapping[" + iValidMappings + "], Invocation["
											+ iTypeInvocation + "] : '" + returnValue.toString() + "'");
								} else {
									// Null return value
									result.ProcessLog.add("Mapping[" + iValidMappings + "], Invocation["
											+ iTypeInvocation + "] : Null");
								}
							}

							iTypeInvocation += 1;
						}
					}
				}

				// Check the amount of valid mappings
				if (iValidMappings == 0) {
					result.result = ProcessResult.eResult.Warning;
					result.exceptionMessage = "[ProcessDataObject]: No valid mapping expression for data object..";
					result.exceptionText = "[ProcessDataObject]: No valid mapping expression for data object..";
				} else {
					// Mappings have been processed, no exception has occurs
					result.result = ProcessResult.eResult.Success;
				}

				// Finished, commit transaction
				return result;

			} catch (InvocationTargetException ex) {
				Throwable e = ex.getTargetException();
				
				if(e != null && e instanceof Exception)
					throw (Exception)e;
				throw ex;
			}
		} catch (ValidationException ex) {
			// Thrown by the handlers indicating the business object is not in a
			// valid state
			// Do not treat this as an error, instead as a warning
			result.result = ProcessResult.eResult.Warning;
			result.exceptionMessage = ex.getMessage();
			result.exceptionText = ex.getMessage();

		} catch (InvalidOperationException ex) {

			result.result = ProcessResult.eResult.Error;
			result.exceptionMessage = ex.getMessage();
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ex.printStackTrace(pw);
			pw.close();
			sw.flush();
			result.exceptionText = sw.toString();
			result.ProcessLog = ex.GetProcessLog();
			sw.close();
		} catch (Exception ex) {

			result.result = ProcessResult.eResult.Error;
			result.exceptionMessage = ex.getMessage();
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ex.printStackTrace(pw);
			pw.close();
			sw.flush();
			result.exceptionText = sw.toString();
			sw.close();
		}

		// Return the result
		return result;
	}
}