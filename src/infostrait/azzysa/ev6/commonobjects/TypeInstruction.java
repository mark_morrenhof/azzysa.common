package infostrait.azzysa.ev6.commonobjects;

import javax.xml.bind.annotation.XmlElement;

/**
 * Holds a single type instruction used to create/update an object within ENOVIA (by DIADataService)
 * @author m.morrenhof
 */
public class TypeInstruction {

    /**
     * Name of the function to invoke
     */
    @XmlElement(required = true, name="MemberName")
    public String MemberName;

    /**
     * Index of instruction within an instruction set
     * @see TypeMappings
     */
    @XmlElement(required = true, name="Sequence")
    public int Sequence;

    /**
     * Parameter collection required to invoke the method
     * @see Parameters
     */
    @XmlElement(required = true, name="Parameters")
    public Parameters Parameters;
    
    /**
     * Constructs a new type instruction
     */
    public TypeInstruction() {
        this.Parameters = new Parameters();
        this.MemberName = "";
        this.Sequence = 0;
    }

}