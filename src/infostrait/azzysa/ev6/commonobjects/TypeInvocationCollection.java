package infostrait.azzysa.ev6.commonobjects;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author m.morrenhof
 */
public class TypeInvocationCollection {
    
    @XmlElement(required = true, name="TypeInvocation")
    public ArrayList<TypeInvocation> invocations;
          
    public TypeInvocationCollection() {
        this.invocations = new ArrayList<TypeInvocation>();
    }
    
}
