package infostrait.azzysa.ev6.commonobjects;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Object used by report service
 *
 * @author t.v.oost
 */
@XmlRootElement
public class Report {

    public String Name;
    public String Queries;
    public String XsltSource;
    public String XsltName;
    public boolean Hidden;

    public Report() {
    }
}