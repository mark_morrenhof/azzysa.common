package infostrait.azzysa.ev6.commonobjects;

/**
 * How to render the report
 * @author t.v.oost
 */
public enum ReportFormat {
    HtmlFragment,
    HtmlDocument,
    WordDocument,
    ExcelDocument
}