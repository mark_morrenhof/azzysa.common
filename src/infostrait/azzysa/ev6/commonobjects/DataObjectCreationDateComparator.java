package infostrait.azzysa.ev6.commonobjects;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class DataObjectCreationDateComparator implements Comparator<DataObject> {
    
    private String _AttributeToCompare;
    
    public DataObjectCreationDateComparator(String attributeToCompare) {
        _AttributeToCompare = attributeToCompare;
    }
    
     /**
     * Internal use only, compare type instructions
     * @param o1
     * @param o2
     * @return 
     */
    @Override
    public int compare(DataObject o1, DataObject o2) {
    	String creationDate1;
    	String creationDate2;
        Date parsedCreationDate1;
        Date parsedCreationDate2;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    	
    	// Get the date values
    	creationDate1 = o1.Attributes.GetValueAsStringWithoutNullOrException(_AttributeToCompare);
    	creationDate2 = o2.Attributes.GetValueAsStringWithoutNullOrException(_AttributeToCompare);

        try {
        	// Parse date string values
            parsedCreationDate1 = df.parse(creationDate1);
            parsedCreationDate2 = df.parse(creationDate2);
        	
            // Compare date values
            return parsedCreationDate1.compareTo(parsedCreationDate2);
        } catch (Exception ex) {
        	// Failed
        	return 0;
        }
    }
}