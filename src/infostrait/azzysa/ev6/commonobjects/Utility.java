package infostrait.azzysa.ev6.commonobjects;

import com.matrixone.apps.domain.DomainObject;
import com.matrixone.apps.domain.DomainRelationship;
import com.matrixone.apps.domain.util.MapList;
import com.matrixone.apps.domain.util.mxAttr;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import matrix.db.AttributeList;
import matrix.db.AttributeType;
import matrix.db.MQLCommand;
import matrix.db.Context;
import matrix.db.JPO;
import matrix.db.MatrixLogWriter;
import matrix.db.Page;
import matrix.util.DateFormatUtil;
import matrix.util.MatrixException;
import matrix.util.StringList;

/**
 * Utility class to provide import enhancements
 * @author m.morrenhof
 */
public class Utility {
    
    /**
     * Reads a property value from the azzysa.properties file that should be located in the WEB-INF\classes directory
     * @param propertyName Name of the property to retrieve
     * @return The value of the property or error message if fails
     */
    public static String getProperty(String propertyName) {
        File propFile;
        String propertyValue;
        try {
            
            // Get property file
            propFile = (new File(Context.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath())).getParentFile().getParentFile().toPath().resolve("classes\\azzysa.properties").toFile();
            
            // Read property file object
            java.util.Properties prop = new java.util.Properties();
            FileInputStream fs = new FileInputStream(propFile);
            prop.load(fs);
            
            // Read the property value
            propertyValue = prop.getProperty(propertyName);
            
        } catch (URISyntaxException ex) {
            propertyValue = ex.getMessage();
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            propertyValue = ex.getMessage();
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            propertyValue = ex.getMessage();
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Finished
        return propertyValue;
    }
    
    /**
     * Return MQL separator character
     * @return 
     */
    public static String getSeparator(){
        return getProperty("azzysa.separator");
    }
    
     /**
     * Return the home location where azzysa platform on the 3DEXPERIENCE platform server has been installed
     * @return 
     */
    public static String getPlatformPath() {
        return getProperty("azzysa.home.dir");
    }
    
     /**
     * Return the path (directory) to store both the key store and the secured parameter file
     * @return 
     */
    public static String getSignedPath() {
        return getProperty("azzysa.home.signed");
    }
    
     /**
     * Return the path (directory) to store temporary data
     * @return 
     */
    public static String getTempPath() {
        return getProperty("azzysa.home.temp");
    }
    
     /**
     * Return the full path to the log file
     * @return 
     */
    public static String getLogFile() {
        return getProperty("azzysa.home.log");
    }
    
    /**
     * Return the value of log severity configured
     * @return 
     */
    public static String getLogSeverity() {
        return getProperty("azzysa.home.log.severity");
    }
    
    /**
     * Function strips all invalid characters from the input string to use it as an object name
     * @param input
     * @return 
     * @throws java.lang.Exception When input value is null or empty or contains only white spaces
     */
    public static String getNameValue(String input) throws Exception {
        if (input == null) {
            throw new Exception("Input value cannot be null for name value!");
        } else if ("".equalsIgnoreCase(input)) {
            throw new Exception("Input value cannot be empty for name value!");
        } else if ("".equalsIgnoreCase(input.trim())) {
            throw new Exception("Input value cannot be contain only whitespaces for name value!");
        } else {
            // Strip invalid characters
            return input.trim();
        }            
    }
    
    /**
     * Function strips all invalid characters from the input string to use it as an object name (without throwing an exception when providing empty string)
     * @param input
     * @return Empty string when containing invalid characters or valid name without leading or trailing white spaces
     * @throws java.lang.Exception
     */
    public static String checkNameValue(String input) throws Exception {
        if (input == null) {
            return "";
        } else if ("".equalsIgnoreCase(input)) {
            return "";
        } else if ("".equalsIgnoreCase(input.trim())) {
            return "";
        } else {
            // Strip invalid characters
            return input.trim();
        }            
    }
    
    /**
     * Pack arguments to a hash map
     * @param object Data object holding all attributes values
     * @param ArgumentsToPack Colon + Semi-column separated string holding attribute key/values mapping which are separated by double colon (e.g. 'Description::{Description}:;Name::{Name}')
     * @return HashMap holding the arguments
     * @throws Exception 
     */
    public static HashMap FillHashMap(DataObject object, String ArgumentsToPack) throws Exception {
        String[] KeyValPairs;
        String[] KeyValPairParsed;
        DataObject objectToUse;
        HashMap returnValue;
        String attributeName;
        String sMacro;
        String sFirstArg;
        String sSecondArg;
        
        // Create default return value
        returnValue = new HashMap();
        
        // Check parameters
        if ("".equalsIgnoreCase(ArgumentsToPack)) {
            // No arguments to pack
            return returnValue;
        }
        
        // Check data object
        if (object == null) {
            objectToUse = new DataObject();            
        } else {
            objectToUse = object;
        }
        
        // Check whether the an inner hashmap is requested (with an ID) or a root hashmap
        // Important NOTE:
        // Root hashmaps are requested from the method PackArguments while
        // inner hashmaps are requested from within this method (hashmap within a hashmap having an key)
        if (ArgumentsToPack.startsWith("$FillHashMap(") && ArgumentsToPack.endsWith(")")) {
            // Hashmap as ROOT - no key value in arguments to pack collection set
            // Get entire method invocation command from mapping            
            sMacro = ArgumentsToPack;

            // Strip method name and brackets from method invocation command resulting in only the parameters collection of the invocation (MUST be two - is not checked)
            sMacro = sMacro.substring(sMacro.indexOf("(") + 1);
            sMacro = sMacro.substring(0, sMacro.length() - 1);

            // Take the first argument from the signature
            sFirstArg = sMacro.substring(0, sMacro.indexOf(",")).trim();
            // Take second argument from signature and skip the first comma and the quote
            sSecondArg = sMacro.substring(sMacro.indexOf(",") + 1).trim();
            sSecondArg = sSecondArg.substring(1).trim();
            // Strip also the last quote
            sSecondArg = sSecondArg.substring(0, sSecondArg.length() - 1);
            
            // Check whether the second argument contains only "" (empty string) -> an empty hashmap is requested
            // If that is the case, make second string empty in order to provide an empty hashmap
            if ("\"\"".equalsIgnoreCase(sSecondArg)) {sSecondArg = "";}

            // Recursive invocation (of this method), check first argument whether to set the data object
            if ("$DATA_OBJECT".equalsIgnoreCase(sFirstArg)) {
                // Set data object to invocation
                returnValue = FillHashMap(objectToUse, sSecondArg);
            } else {
                // Anything else : set null
                returnValue = FillHashMap(null, sSecondArg);
            }
            
        } else {
            // Split string into key/value pairs
            KeyValPairs = ArgumentsToPack.split(":;");
            for (String KeyValuePair : KeyValPairs) {
                KeyValPairParsed = KeyValuePair.split("::");

                // Check if key-value pair is parsed succesfully
                if (KeyValPairParsed.length == 2) {
                    // Add key value pair

                    if (KeyValPairParsed[1].startsWith("{") && KeyValPairParsed[1].endsWith("}")) {
                        // Take data object attribute
                        // Check whether the object has this attribute
                        attributeName = KeyValPairParsed[1].substring(1);
                        attributeName = attributeName.substring(0, attributeName.length() - 1);
                        if (objectToUse.Attributes.ContainsAttribute(attributeName)) {
                            // Use attribute value
                            returnValue.put(KeyValPairParsed[0], objectToUse.Attributes.GetValueAsString(attributeName));
                        } else {
                            // Set empty string
                            returnValue.put(KeyValPairParsed[0], "");
                        }

                    } else if (KeyValPairParsed[1].startsWith("$FillHashMap(") && KeyValPairParsed[1].endsWith(")")) {
                        // Fill INNER Hashmap collection holding object attributes and other values (inner hashmap or plain text)

                        // Get entire method invocation command from mapping
                        sMacro = KeyValPairParsed[1];

                        // Strip method name and brackets from method invocation command resulting in only the parameters collection of the invocation (MUST be two - is not checked)
                        sMacro = sMacro.substring(sMacro.indexOf("(") + 1);
                        sMacro = sMacro.substring(0, sMacro.length() - 1);

                        // Take the first argument from the signature
                        sFirstArg = sMacro.substring(0, sMacro.indexOf(",")).trim();
                        sSecondArg = sMacro.substring(sMacro.indexOf(",") + 1).trim();

                        // Check whether the second argument contains only "" (empty string) -> an empty hashmap is requested
                        // If that is the case, make second string empty in order to provide an empty hashmap
                        if ("\"\"".equalsIgnoreCase(sSecondArg)) {sSecondArg = "";}

                        // Recursive invocation (of this method), check first argument whether to set the data object
                        if ("$DATA_OBJECT".equalsIgnoreCase(sFirstArg)) {
                            // Set data object to invocation
                            returnValue.put(KeyValPairParsed[0], FillHashMap(objectToUse, sSecondArg));
                        } else {
                            // Anything else : set null
                            returnValue.put(KeyValPairParsed[0], FillHashMap(null, sSecondArg));
                        }

                    } else {
                        // Just set the value
                        returnValue.put(KeyValPairParsed[0], KeyValPairParsed[1]);
                    }

                } else if (KeyValPairParsed.length == 1) {
                    // Add only the first string returned
                    returnValue.put(KeyValPairParsed[0], "");
                } else {
                    // Do not add anything
                }

                attributeName = "";
                KeyValPairParsed = null;
            }
        }
        
        // Finished        
        return returnValue;
        
    }

    /**
     * Pack arguments (e.g. for use with JPO invoking) to string array
     * @param object Data object holding all attributes values
     * @param ArgumentsToPack Colon + Semi-column separated string holding attribute key/values mapping which are separated by double colon (e.g. 'Description::{Description}:;Name::{Name}')
     * @return String array holding the arguments
     */
    public static String[] PackArguments(DataObject object, String ArgumentsToPack) throws Exception {
        String[] returnValue;
        HashMap collection;
       
        // Fill hashmap with arguments
        collection = FillHashMap(object, ArgumentsToPack);
        
        // Convert array into string array
        returnValue = JPO.packArgs(collection);
        return returnValue;
        
    }
    
    /**
     * Invoke method invokes the JPO in eMatrix Kernel, returning a status (not fully implemented yet)
     * @param context Connected Matrix context
     * @param jpoName The JPO name
     * @param constructorArgs The arguments to pass to the constructor
     * @param methodName The methodName to call, MatrixMain if null
     * @param invokeArgs The arguments to pass to the method
     * @return 
     */
    public static int InvokeJPO(Context context, String jpoName, String[] constructorArgs, String methodName, String[] invokeArgs) throws Exception {
        return JPO.invoke(context, jpoName, invokeArgs, methodName, invokeArgs);
    }

    /**
     * Invoke method invokes the JPO in eMatrix Kernel, returning an Object. If the method fails Exception is thrown (not fully implemented yet)
     * @param context Connected Matrix context
     * @param jpoName The JPO name
     * @param constructorArgs The arguments to pass to the constructor
     * @param methodName The methodName to call, MatrixMain if null
     * @param invokeArgs The arguments to pass to the method
     * @param returnType The return class type
     * @return 
     */
    public static Object InvokeJPO(Context context, String jpoName, String[] constructorArgs, String methodName, String[] invokeArgs, Class returnType) throws Exception {
        return JPO.invoke(context, jpoName, invokeArgs, methodName, invokeArgs, returnType);
    }
    
    /**
     * Collects all attributes of the DomainObject that are defined in the select list and return them within a dataobject
     * @param context Connected Matrix context
     * @param object Object to convert
     * @param select Attributes to select
     * @param level Level of the domain object (set to null string to not add a level attribute)
     * @return
     * @throws Exception 
     */
    public static DataObject DomainObjectToDataObject(Context context, DomainObject object, StringList select, String level) throws Exception {
        DataObject returnValue;
        ObjectAttribute attr;
        
        // Create new dataobject
        returnValue = new DataObject();
        
        for (Object selectable : select) {
            attr = new ObjectAttribute();
            
            // Get name
            attr.Name = (String) selectable;
                    
            // Get value
            attr.Value = object.getInfo(context, attr.Name);
            
            // Add to data object
            returnValue.Attributes.ObjectAttribute.add(attr);
        }

        // Check whether a level attribute is requested
        if (level != null) {
            attr = new ObjectAttribute();
            attr.Name = "level";
            attr.Value = level;
            returnValue.Attributes.ObjectAttribute.add(attr);
        }
        
        // Finished
        return returnValue;
    }
    
    /**
     * Collects all hashtable entries and return them within a dataobject
     * @param context Connected Matrix context
     * @param table Table to convert
     * @param level
     * @return Level value (set to null string to not add a level attribute)
     * @throws Exception 
     */
    public static DataObject HashTableToDataObject(Context context, Hashtable table, String level) throws Exception {
        DataObject returnValue;
        ObjectAttribute attr;
                
        // Get all attributes
        returnValue = new DataObject();
        for (Object keyVal : table.entrySet()) {
            Map.Entry<Object, Object> entry  = (Map.Entry<Object, Object>)keyVal;
            attr = new ObjectAttribute();
            attr.Name = (String) entry.getKey();
            attr.Value = (String) entry.getValue();
            returnValue.Attributes.ObjectAttribute.add(attr);
        }

        // Check whether a level attribute is requested
        if (level != null) {
            attr = new ObjectAttribute();
            attr.Name = "level";
            attr.Value = level;
            returnValue.Attributes.ObjectAttribute.add(attr);
        }
        
        // Finished
        return returnValue;
    }
    
    /**
     * Queries the ENOVIA database in order to retrieve the internal object id
     * @param context Connected Matrix context
     * @param type ENOVIA type name
     * @param name Object name (function returns empty string on invalid name value (empty string))
     * @param revision Revision mask
     * @return
     * @throws MatrixException 
     */
    public static String QueryObjectId(Context context, String type, String name, String revision) throws Exception, MatrixException{        
        boolean ret;
        MQLCommand theCommand;
        String sCommand;
        String[] CommandArgs;
        String sResult;
        String sObjectId;        
      
        // Warning #1500555 Query specifies empty name but system settings do not allow such names
        // Prevent querying on empty names
        if ("".equalsIgnoreCase(checkNameValue(name))) {
            // Empty name, return empty string
            return "";
        }
        
        // Initialize MQL command
        theCommand = new MQLCommand();
        sCommand = "temp query bus $1 $2 $3 select $4 dump $5;";
        
        // Set parameters
        CommandArgs = new String[5];
        CommandArgs[0] = type;
        CommandArgs[1] = name;
        CommandArgs[2] = revision;
        CommandArgs[3] = "id";
        CommandArgs[4] = getSeparator();

        // Execute query          
        ret = theCommand.executeCommand(context, sCommand, CommandArgs);

        // Retrieve query result   
        sResult = theCommand.getResult();

        // Parse query result (retrieve ENOVIA Object id)          
        sObjectId = sResult.trim().split(getSeparator())[(sResult.trim().split(getSeparator()).length - 1)];

        // Finished, return result
        return sObjectId;
    }

    /**
     * Queries the ENOVIA database in order to retrieve the internal object id by external 'SmarTeam identifier
     * @param context Connected Matrix context
     * @param ClassId SmarTeam Class Id
     * @param ObjectId SmarTeam Object Id
     * @param throwEx Throw exception when object cannot be found
     * @return ENOVIA Object Id
     * @throws MatrixException 
     */
    public static String QueryObjectIdById(Context context, java.lang.String ClassId, java.lang.String ObjectId, java.lang.Boolean throwEx) throws Exception {        
        boolean ret;
        MQLCommand theCommand;
        String sCommand;
        String[] CommandArgs;
        String sResult;
        String sObjectId;
        int iObjectId;
        
        // Check whether the object id is not negative or zero
        iObjectId = Integer.parseInt(ObjectId);
        if (iObjectId <= 0) {
            // Invalid object identifier
            throw new ObjectIdentifierInvalidException();
        }
        
        // Initialize MQL command
        theCommand = new MQLCommand();
        sCommand = "temp query bus $1 $2 $3 where '$4' select $5 dump $6;";

         // Set parameters
        CommandArgs = new String[6];
        CommandArgs[0] = "*";
        CommandArgs[1] = "*";
        CommandArgs[2] = "*";
        CommandArgs[3] = "attribute[ClassId] == " + ClassId + " and attribute[ObjectId] == " + ObjectId;
        CommandArgs[4] = "id";
        CommandArgs[5] = getSeparator();

        // Execute query          
        ret = theCommand.executeCommand(context, sCommand, CommandArgs);

        // Retrieve query result   
        sResult = theCommand.getResult();
        
        // Check result        
        if ((sResult.equalsIgnoreCase("")) && throwEx) {
            // No results
            throw new Exception("Unable to find object [ClassId: " + ClassId + 
                    " / ObjectId:" + ObjectId + "]!");
        }

        // Parse query result (retrieve ENOVIA Object id)          
        sObjectId = sResult.trim().split(getSeparator())[(sResult.trim().split(getSeparator()).length - 1)];

        // Finished, return result
        return sObjectId;
    }
   
    /**
     * Queries the ENOVIA database in order to retrieve the internal object id by external 'SmarTeam object identifier and ENOVIA type name
     * @param context Connected Matrix context
     * @param type Type name of the ENOVIA object searched for
     * @param ObjectId SmarTeam Object Identifier of the object
     * @param throwEx Throw exception when object cannot be found
     * @return
     * @throws Exception 
     */
    public static String QueryObjectIdByObjId(Context context, java.lang.String type, java.lang.String ObjectId, java.lang.Boolean throwEx) throws Exception {
        boolean ret;
        MQLCommand theCommand;
        String sCommand;
        String[] CommandArgs;
        String sResult;
        String sObjectId;
        int iObjectId;
        
        // Check whether the object id is not negative or zero
        iObjectId = Integer.parseInt(ObjectId);
        if (iObjectId <= 0) {
            // Invalid object identifier
            throw new ObjectIdentifierInvalidException();
        }
        
        // Initialize MQL command
        theCommand = new MQLCommand();
        sCommand = "temp query bus $1 $2 $3 where '$4' select $5 dump $6;";

         // Set parameters
        CommandArgs = new String[6];
        CommandArgs[0] = type;
        CommandArgs[1] = "*";
        CommandArgs[2] = "*";
        CommandArgs[3] = "attribute[ObjectId] == " + ObjectId;
        CommandArgs[4] = "id";
        CommandArgs[5] = getSeparator();

        // Execute query          
        ret = theCommand.executeCommand(context, sCommand, CommandArgs);

        // Retrieve query result   
        sResult = theCommand.getResult();
        
        // Check result        
        if ((sResult.equalsIgnoreCase("")) && throwEx) {
            // No results
            throw new Exception("Unable to find object [Type: " + type + 
                    " / ObjectId:" + ObjectId + "]!");
        }

        // Parse query result (retrieve ENOVIA Object id)          
        sObjectId = sResult.trim().split(getSeparator())[(sResult.trim().split(getSeparator()).length - 1)];

        // Finished, return result
        return sObjectId;
        
    }
    
    /**
     * Retrieve multiple object identifiers based on type name revision
     * @param context Connected Matrix context
     * @param type ENOVIA type name
     * @param name Object name (function returns empty string array on invalid name value (empty string))
     * @param revision Revision mask
     * @return 
     * @throws Exception 
     */
    public static String[] QueryObjectIds(Context context, String type, String name, String revision) throws Exception {
        boolean ret;
        MQLCommand theCommand;
        String sCommand;
        String[] CommandArgs;
        String sResult;
        String[] sObjectIds;
        int idx;
        
        // Warning #1500555 Query specifies empty name but system settings do not allow such names
        // Prevent querying on empty names
        if ("".equalsIgnoreCase(checkNameValue(name))) {
            // Empty name, return empty string
            return new String[0];
        }
              
        // Initialize MQL command
        theCommand = new MQLCommand();
        sCommand = "temp query bus $1 $2 $3 select $4 dump $5;";

        // Set parameters
        CommandArgs = new String[5];
        CommandArgs[0] = type;
        CommandArgs[1] = name;
        CommandArgs[2] = revision;
        CommandArgs[3] = "id";
        CommandArgs[4] = getSeparator();

        // Execute query          
        ret = theCommand.executeCommand(context, sCommand, CommandArgs);

        // Retrieve query result   
        sResult = theCommand.getResult();
        
        // Parse the result
        String textStr[] = sResult.split("\\r?\\n");
        sObjectIds = new String[textStr.length];
        idx = 0;
        for(String sLine : textStr) {
            sObjectIds[idx] = sLine.trim().split(getSeparator())[(sLine.trim().split(getSeparator()).length - 1)];;
            idx++;
        }
        
        // Finished
        return sObjectIds;
    }

    /**
     * Retrieve single object based on type name revision
     * @param context Connected Matrix context
     * @param type ENOVIA type name
     * @param name ENOVIA object name
     * @param revision Object revision
     * @return
     * @throws Exception 
     */
    public static DomainObject QueryObject(Context context, String type, String name, String revision) throws Exception {
        String sObjectId;
        DomainObject result;
        
        sObjectId = QueryObjectId(context, type, name, revision);
        if (sObjectId != null) {
            if (! "".equalsIgnoreCase(sObjectId)) {
                result = new DomainObject(sObjectId);
            } else {
                // Object not found
                result = null;
            }
        } else {
            // Object not found
            result = null;
        }
        
        // Finished
        return result;
            
        }

    /**
     * Retrieve single object based on SmarTeam identifier
     * @param context Connected Matrix context
     * @param ClassId SmarTeam Class Id
     * @param ObjectId SmarTeam Object Id
     * @return DomainObject
     * @throws Exception 
     */
    public static DomainObject QueryObjectById(Context context, java.lang.String ClassId, java.lang.String ObjectId) throws Exception {
        DomainObject returnValue;
        String sObjectId;
        
        // First retrieve object id by searching on SmarTeam object identifier and class identifier
        sObjectId = QueryObjectIdById(context, ClassId, ObjectId, Boolean.TRUE);
        
        // Construct Domain Object
        returnValue = new DomainObject(sObjectId);
        
        // Finished
        return returnValue;
        
    }

    /**
     * Retrieve single object based on SmarTeam object identifier and ENOVIA type
     * @param context Connected Matrix context
     * @param RefType ENOVIA type name
     * @param ObjectId SmarTeam Object Id
     * @return DomainObject
     * @throws Exception 
     */
    public static DomainObject QueryObjectByRefType(Context context, java.lang.String RefType, java.lang.String ObjectId) throws Exception {
        DomainObject returnValue;
        String sObjectId;
        
        // First retrieve object id by searching on SmarTeam object identifier and class identifier        
        sObjectId = QueryObjectIdByObjId(context, RefType, ObjectId, Boolean.TRUE);
        
        // Construct Domain Object
        returnValue = new DomainObject(sObjectId);
        
        // Finished
        return returnValue;
        
    }
    
    /**
     * Queries the ENOVIA database in order to update an object's attributes by external SmarTeam identifier
     * @param context Connected Matrix context
     * @param ClassId SmarTeam Class Id
     * @param ObjectId SmarTeam Object Id
     * @param attributesToUpdate Attribute values of the object to update
     */
    public static void UpdateObjectById(Context context, java.lang.String ClassId, java.lang.String ObjectId, AttributeList attributesToUpdate) throws Exception {
        DomainObject objectToUpdate;
        
        // Get Domain Object
        objectToUpdate = QueryObjectById(context, ClassId, ObjectId);
        
        // Update object
        objectToUpdate.setAttributeValues(context, attributesToUpdate);
        
        // Finished
    }

    /**
     * Queries the ENOVIA database in order to update an object's attributes by external 'SmarTeam identifier
     * @param context Connected Matrix context
     * @param type Object type name of object to update
     * @param name Name of the object to update
     * @param revision Object revision of the object to update
     * @param attributesToUpdate Attribute values of the object to update
     */
    public static void UpdateObjectByTNR(Context context, java.lang.String type, java.lang.String name, java.lang.String revision, AttributeList attributesToUpdate) throws Exception {
        DomainObject objectToUpdate;
        
        // Get Domain Object
        objectToUpdate = QueryObject(context, type, name, revision);
        
        // Update object
        objectToUpdate.setAttributeValues(context, attributesToUpdate);
        
        // Finished
    }    
    
    /**
     * Updates an existing object attribute in ENOVIA with the referenced object's attribute
     * @param context Connected Matrix Context
     * @param ObjectId ENOVIA object id of the object to update
     * @param attribute Attribute name of the object to update
     * @param RefClassId Referenced class identifier
     * @param RefObjectId Referenced object identifier
     * @param RefAttribute Attribute name of the referenced object to use it's value of
     * @return Object Identifier object the updated
     * @throws Exception 
     */
    public static String ResolveReferenceToClass(Context context, java.lang.String ObjectId, java.lang.String attribute, java.lang.String RefClassId, java.lang.String RefObjectId, java.lang.String RefAttribute) throws Exception {
        DomainObject objectToRetrieve;
        DomainObject objectToUpdate;
        
        // First get object
        objectToRetrieve = QueryObjectById(context, RefClassId, RefObjectId);
        
        // Get object to update
        objectToUpdate = new DomainObject(ObjectId);
        
        // Check attribute/basic
        if ("name".equalsIgnoreCase(attribute)) {
            // Update the name
            objectToUpdate.setName(context, objectToRetrieve.getInfo(context, RefAttribute));
        } else if ("description".equalsIgnoreCase(attribute)) {
            // Update description
            objectToUpdate.setDescription(context, objectToRetrieve.getInfo(context, RefAttribute));
        } else {
            // Update attribute of object
            objectToUpdate.setAttributeValue(context, attribute, objectToRetrieve.getInfo(context, RefAttribute));
        }
            
        // Finished
        return objectToUpdate.getId(context);
        
    }

    /**
     * Updates an existing object attribute in ENOVIA with the referenced object's attribute
     * @param context Connected Matrix Context
     * @param ObjectId ENOVIA object id of the object to update
     * @param attribute Attribute name of the object to update
     * @param RefObjectId Referenced object identifier
     * @param RefAttribute Attribute name of the referenced object to use it's value of
     * @return Object Identifier object the updated
     * @throws Exception 
     */
    public static String ResolveReferenceToClassByRefType(Context context, java.lang.String ObjectId, java.lang.String attribute, java.lang.String RefType, java.lang.String RefObjectId, java.lang.String RefAttribute) throws Exception {
        DomainObject objectToRetrieve;
        DomainObject objectToUpdate;
        
        // First get object
        objectToRetrieve = QueryObjectByRefType(context, RefType, RefObjectId);
        
        // Get object to update
        objectToUpdate = new DomainObject(ObjectId);
        
        // Check attribute/basic
        if ("name".equalsIgnoreCase(attribute)) {
            // Update the name
            objectToUpdate.setName(context, objectToRetrieve.getInfo(context, RefAttribute));
        } else if ("description".equalsIgnoreCase(attribute)) {
            // Update description
            objectToUpdate.setDescription(context, objectToRetrieve.getInfo(context, RefAttribute));
        } else {
            // Update attribute of object
            objectToUpdate.setAttributeValue(context, attribute, objectToRetrieve.getInfo(context, RefAttribute));
        }
        
        // Finished
        return objectToUpdate.getId(context);
        
    }
    
    /**
     * Connect two of more ENOVIA objects together and update the created connection attributes
     * @param context Connected Matrix context
     * @param FromObjectId ENOVIA object identifier of the 'from' object
     * @param ToObjectId ENOVIA object identifier of the 'to' object
     * @param relationshipType Name of the relationship type as known within ENOVIA
     * @param linkAttributes Collection of attribute name and values that is updated to the created connection
     * @return Last created domain relationship (if any)
     */
    public static DomainRelationship ConnectObject(Context context, String FromObjectId, String ToObjectId, String relationshipType, AttributeList linkAttributes) throws Exception {
        DomainRelationship rel;
        DomainObject fromObject;
        DomainObject toObject;   
        matrix.util.StringList errorMessages;        
        
        // Log for troubleshoot/debug purposes            
        errorMessages = new matrix.util.StringList();
        errorMessages.addElement("[ConnectObject] : Retrieve Type Mapping");

        try {
            // Initialize return value to null to prevent compile errors
            rel = null;

            // Get objects to connect
            fromObject = new DomainObject(FromObjectId);
            errorMessages.addElement("[ConnectObject] : From Object retrieved (" + FromObjectId + ")");
            toObject = new DomainObject(ToObjectId);
            errorMessages.addElement("[ConnectObject] : To Object retrieved (" + ToObjectId + ")");

            // Connect objects
            errorMessages.addElement("[ConnectObject] : Connect object [" 
                    + fromObject.getInfo(context, "type") + ", " + fromObject.getInfo(context, "name") 
                    + ", " + fromObject.getInfo(context, "revision") + "] to object ["
                    + toObject.getInfo(context, "type") + ", " + toObject.getInfo(context, "name") 
                    + ", " + toObject.getInfo(context, "revision") + "] using relation [" 
                    + relationshipType + "]");
            rel = DomainRelationship.connect(context, fromObject, relationshipType, toObject);
            errorMessages.addElement("[ConnectObject] : Objects connected");

            // Update attributes
            for (int iAttr = 0; iAttr < linkAttributes.size(); iAttr++){
                errorMessages.addElement("      > Attribute #" + iAttr + " [" + 
                        ((matrix.db.Attribute)linkAttributes.get(iAttr)).getName() + "], Value ["
                        + ((matrix.db.Attribute)linkAttributes.get(iAttr)).getValue() + "]");
            }
            
            rel.setAttributeValues(context, linkAttributes);      
            errorMessages.addElement("[ConnectObject] : Connection attributes updated...");

            // Finished
            return rel;
        } catch (Exception ex) {
            // Create log writer
            MatrixLogWriter logWriter;
            logWriter = new MatrixLogWriter(context);
            
            // Log error messages
            logWriter.append("[ConnectObject]: An exception occured: " + ex.toString());
            for (int iLine = 0; iLine < errorMessages.size(); iLine++){
                logWriter.append("      > " + (String)errorMessages.get(iLine));
            }
            
            throw new Exception(ex.toString());
        }
    }
    
    /**
     * Connect two SmarTeam objects that should be query-able by ObjectId and ClassId, specifying the from class id and to class id.
     * If either on of the class id's cannot be found in the connectAttribute parameter no exception is thrown an no connection is created
     * @param context Connected Matrix context
     * @param FromClassId Class id of the object that is connected on the From side of the connection
     * @param ToClassId Class id of the object that is connected on the To side of the connection
     * @param relationshipType Name of the relationship type as known within ENOVIA
     * @param connectAttributes Attribute list holding the connect attributes: OBJECT_ID1, CLASS_ID1, OBJECT_ID2, CLASS_ID2
     * @param attributes Collection of attribute name and values that is updated to the created connection
     * @return Created domain relationship (if any)
     * @throws Exception If both class id's are stored within the attribute list and cannot be found in ENOVIA 
     * @throws Exception If parameters FromClassId, ToClassId, connectAttributes and relationshipType are null or empty
     */
    public static DomainRelationship ConnectSmarTeamObjects(Context context, java.lang.String FromClassId, java.lang.String ToClassId, String relationshipType, AttributeList connectAttributes, AttributeList attributes) throws Exception {
        DomainRelationship rel;
        DomainObject fromObject;
        DomainObject toObject;   
        matrix.util.StringList errorMessages; 
        matrix.db.Attribute attObjectId1;
        matrix.db.Attribute attClassId1;
        matrix.db.Attribute attObjectId2;
        matrix.db.Attribute attClassId2;
        
        // Log for troubleshoot/debug purposes            
        errorMessages = new matrix.util.StringList();
        
        // Check parameters
        errorMessages.addElement("[ConnectSmarTeamObjects] : Check parameters");
        if ("".equalsIgnoreCase(FromClassId) || FromClassId == null) {
            throw new Exception("FromClassId cannot be empty");
        }
        if ("".equalsIgnoreCase(ToClassId) || ToClassId == null) {
            throw new Exception("ToClassId cannot be empty");
        }
        if (connectAttributes == null) {
            throw new Exception("connectAttributes cannot be null");
        }
        
        try {
            // Check whether connected attributes are within the connect attribute list
            errorMessages.addElement("[ConnectSmarTeamObjects] : Check whether connected attributes are within the connect attribute list");
            
            // Collect required attributes from the connectAttributes list
            attObjectId1 = null;
            attClassId1 = null;
            attObjectId2 = null;
            attClassId2 = null;
            for (Object att : connectAttributes) {
                if (((matrix.db.Attribute)att).getName().equalsIgnoreCase("OBJECT_ID1")) {
                    attObjectId1 = (matrix.db.Attribute)att;
                    errorMessages.addElement("[ConnectSmarTeamObjects] : Attribute [" + ((matrix.db.Attribute)att).getName() + "] from connectAttributes collection retrieved");
                } else if (((matrix.db.Attribute)att).getName().equalsIgnoreCase("CLASS_ID1")) {
                    attClassId1 = (matrix.db.Attribute)att;
                    errorMessages.addElement("[ConnectSmarTeamObjects] : Attribute [" + ((matrix.db.Attribute)att).getName() + "] from connectAttributes collection retrieved");
                } else if (((matrix.db.Attribute)att).getName().equalsIgnoreCase("OBJECT_ID2")) {
                    attObjectId2 = (matrix.db.Attribute)att;
                    errorMessages.addElement("[ConnectSmarTeamObjects] : Attribute [" + ((matrix.db.Attribute)att).getName() + "] from connectAttributes collection retrieved");
                } else if (((matrix.db.Attribute)att).getName().equalsIgnoreCase("CLASS_ID2")) {
                    attClassId2 = (matrix.db.Attribute)att;
                    errorMessages.addElement("[ConnectSmarTeamObjects] : Attribute [" + ((matrix.db.Attribute)att).getName() + "] from connectAttributes collection retrieved");
                } else {
                    // Unknown attribute, not used within this collection
                    errorMessages.addElement("[ConnectSmarTeamObjects] : Unknown attribute in connectAttributes collection " + ((matrix.db.Attribute)att).getName());
                }
            }

            // Check if all required attributes are retrieved.
            errorMessages.addElement("[ConnectSmarTeamObjects] : Check if all required attributes are retrieved...");
            if (attObjectId1 == null) {
                throw new Exception("Missing attribute in connectAttributes collection: OBJECT_ID1");
            }
            if (attClassId1 == null) {
                throw new Exception("Missing attribute in connectAttributes collection: CLASS_ID1");
            }
            if (attObjectId2 == null) {
                throw new Exception("Missing attribute in connectAttributes collection: OBJECT_ID2");
            }
            if (attClassId2 == null) {
                throw new Exception("Missing attribute in connectAttributes collection: CLASS_ID2");
            }
                        
            // Initialize return value to null to prevent compile errors
            rel = null;
            
            // Retrieve [From] object
            errorMessages.addElement("[ConnectSmarTeamObjects] : Retrieve [From] object...");
            if (FromClassId.equalsIgnoreCase(attClassId1.getValue())) {
                // OBJECT_ID1 and CLASS_ID1 represents object at the from side of the connection
                errorMessages.addElement("[ConnectSmarTeamObjects] : OBJECT_ID1 and CLASS_ID1 represents object at the from side of the connection...");
                fromObject = QueryObjectById(context, attClassId1.getValue(), attObjectId1.getValue());
            } else if (FromClassId.equalsIgnoreCase(attClassId2.getValue())) {
                // OBJECT_ID2 and CLASS_ID2 represents object at the from side of the connection
                errorMessages.addElement("[ConnectSmarTeamObjects] : OBJECT_ID2 and CLASS_ID2 represents object at the from side of the connection...");
                fromObject = QueryObjectById(context, attClassId2.getValue(), attObjectId2.getValue());
            } else {
                // No FROM objec defined in the connectAttributes collection, return null
                return null;
            }

            // Retrieve [To] object
            errorMessages.addElement("[ConnectSmarTeamObjects] : Retrieve [To] object...");
            if (ToClassId.equalsIgnoreCase(attClassId1.getValue())) {
                // OBJECT_ID1 and CLASS_ID1 represents object at the to side of the connection
                errorMessages.addElement("[ConnectSmarTeamObjects] : OBJECT_ID1 and CLASS_ID1 represents object at the from side of the connection...");
                toObject = QueryObjectById(context, attClassId1.getValue(), attObjectId1.getValue());
            } else if (ToClassId.equalsIgnoreCase(attClassId2.getValue())) {
                // OBJECT_ID2 and CLASS_ID2 represents object at the to side of the connection
                errorMessages.addElement("[ConnectSmarTeamObjects] : OBJECT_ID2 and CLASS_ID2 represents object at the from side of the connection...");
                toObject = QueryObjectById(context, attClassId2.getValue(), attObjectId2.getValue());
            } else {
                // No To object defined in the connectAttributes collection, return null
                return null;
            }
            
            // Check if both objecs are retrieved (should be at this point)
            if (fromObject == null) {
                errorMessages.addElement("[ConnectSmarTeamObjects] : [From] object cannot be found!");
                throw new Exception("[ConnectSmarTeamObjects] : [From] object cannot be found!");
            }
            if (toObject == null) {
                errorMessages.addElement("[ConnectSmarTeamObjects] : [To] object cannot be found!");
                throw new Exception("[ConnectSmarTeamObjects] : [To] object cannot be found!");
            }

            // Connect objects
            errorMessages.addElement("[ConnectObject] : Connect object [" 
                    + fromObject.getInfo(context, "type") + ", " + fromObject.getInfo(context, "name") 
                    + ", " + fromObject.getInfo(context, "revision") + "] to object ["
                    + toObject.getInfo(context, "type") + ", " + toObject.getInfo(context, "name") 
                    + ", " + toObject.getInfo(context, "revision") + "] using relation [" 
                    + relationshipType + "]");
            rel = DomainRelationship.connect(context, fromObject, relationshipType, toObject);
            errorMessages.addElement("[ConnectObject] : Objects connected");

            // Check whether to update the link attributes
            if (attributes != null) {
                // Update attributes
                for (int iAttr = 0; iAttr < attributes.size(); iAttr++){
                    errorMessages.addElement("      > Attribute #" + iAttr + " [" + 
                            ((matrix.db.Attribute)attributes.get(iAttr)).getName() + "], Value ["
                            + ((matrix.db.Attribute)attributes.get(iAttr)).getValue() + "]");
                }

                rel.setAttributeValues(context, attributes);      
                errorMessages.addElement("[ConnectObject] : Connection attributes updated...");
            }

            // Finished
            return rel;
        } catch (Exception ex) {
            // Create log writer
            MatrixLogWriter logWriter;
            logWriter = new MatrixLogWriter(context);
            
            // Log error messages
            logWriter.append("[ConnectObject]: An exception occured: " + ex.toString());
            for (int iLine = 0; iLine < errorMessages.size(); iLine++){
                logWriter.append("      > " + (String)errorMessages.get(iLine));
            }
            
            throw new Exception(ex.toString());
        }
    }
    
    /**
     * Connect two of more ENOVIA objects together and update the created connection attributes
     * @param context Connected Matrix context
     * @param ObjectId ENOVIA object id
     * @param ObjectIds Collection of ENOVIA object id's of the to objects
     * @param IsFrom Boolean value (true/false) indicating whether the parameter ObjectId represents the from or to side of the relation
     * @param relationshipType Name of the relationship type as known within ENOVIA
     * @param linkAttributes Collection of attribute name and values that is updated to the created connection
     * @return Last created domain relationship (if any)
     */
    public static DomainRelationship ConnectObjects(Context context, String ObjectId, String[] ObjectIds, Boolean IsFrom, String relationshipType,AttributeList linkAttributes) throws Exception {
        DomainRelationship rel;
        DomainObject fromObject;
        DomainObject toObject;   
        
        // Initialize return value to null to prevent compile errors
        rel = null;
        
        // Check the from/to connection
        if (IsFrom) {
            fromObject = new  DomainObject(ObjectId);
            
            for(String sRelatedId : ObjectIds) {
                // Get related object
                toObject = new DomainObject(sRelatedId);
                
                // Connect objects
                rel = DomainRelationship.connect(context, toObject, relationshipType, fromObject);
                
                // Update attributes
                rel.setAttributeValues(context, linkAttributes);
            }
        } else {
            toObject = new  DomainObject(ObjectId);

            for(String sRelatedId : ObjectIds) {
                // Get related object
                fromObject = new DomainObject(sRelatedId);
                
                // Connect objects
                rel = DomainRelationship.connect(context, fromObject, relationshipType, toObject);
                
                // Update attributes
                rel.setAttributeValues(context, linkAttributes);
                
            }
        }
        
        // Finished
        return rel;
    }
    
    /**
     * Function creates a new object or revises an existing object when an object with the specified name already exists
     * @param context Connected Matrix context
     * @param type Type name (only applicable for new objects - not when revising an object)
     * @param name Name of the object to create or revise (only applicable for new objects - not when revising an object)
     * @param revision Revision of the new object or next revision
     * @param state Set the state of the new object or new revision
     * @param policy The policy of the object (only applicable for new objects - not when revising an object)
     * @param vault The vault of the object (only applicable for new objects - not when revising an object)
     * @param attributes Attribute values of the object
     * @return
     * @throws Exception 
     */
    public static String CreateOrReviseObject(Context context, String type, String name, String revision, String state, String policy, String vault, AttributeList attributes) throws Exception {
        return CreateOrReviseObject(context, type, name, revision, state, null, policy, vault, attributes);
    }

    /**
     * Function creates a new object or revises an existing object when an object with the specified name already exists
     * @param context Connected Matrix context
     * @param type Type name (only applicable for new objects - not when revising an object)
     * @param name Name of the object to create or revise (only applicable for new objects - not when revising an object)
     * @param revision Revision of the new object or next revision
     * @param state Set the state of the new object or new revision
     * @param description Set the description of the new object
     * @param policy The policy of the object (only applicable for new objects - not when revising an object)
     * @param vault The vault of the object (only applicable for new objects - not when revising an object)
     * @param attributes Attribute values of the object
     * @return
     * @throws Exception 
     */
    public static String CreateOrReviseObject(Context context, String type, String name, String revision, String state, String description, String policy, String vault, AttributeList attributes) throws Exception {
        MapList searchResult;
        StringList querySelect;
        String sResult;
        DomainObject newObject;
        String sObjectId;
        
        // First check whether part already exists in the database
        querySelect = new StringList();
        querySelect.addElement("Id");        
        searchResult = DomainObject.findObjects(context, type, getNameValue(name), "*", "*", "*", "", true, querySelect);
        
        // Check query result
        if (searchResult.size() == 0) {
            // New object
            newObject = new DomainObject();
            newObject.createObject(context, type, getNameValue(name), revision, policy, vault);           
        } else {
            // New revision of part
            sObjectId = (String)((HashMap)searchResult.get(0)).get("id");
            newObject = new DomainObject(sObjectId);            
            newObject = new DomainObject(newObject.reviseObject(context, revision, true));
        }
        
        if (description != null && !"".equalsIgnoreCase(description)) {
            newObject.setDescription(context, description);
        }
        
        // Update attributes
        newObject.setState(context, state);
        newObject.setAttributeValues(context, attributes);
        
        // Finished
        sResult = newObject.getId(context);
        return sResult;
    }
     
    /**
     * Converts object attribute collection to matrix AttributeList
     * @param context
     * @param dataObject
     * @param sMacro String value holds the attribute mapping. 
     * This is a comma separated list of attribute mapping.
     * Each attribute mapping consists of a source attribute name (as known in the dataObject, and a target attribute name (as known in ENOVIA)
     * It also can contain one attribute name, this is the case when both the source and ENOVIA shares the same attribute name (also in next example)
     * 
     * Example of such a macro is: ${ATTRIBUTES(CN_DESCRIPTION => Description, CN_UOM => Unit Of Measure, Key.CompanyId => OrganizationID, EmailAddress)}
     * 
     * @return
     * @throws Exception 
     */
    public static AttributeList ToAttributeList(Context context, DataObject dataObject, String sMacro) throws Exception {
        AttributeList _Result;
        HashMap attrMap;
        String sInnerMacro;
        SimpleDateFormat formatter;
        matrix.db.Attribute attr;
        
        // Parse macro
        attrMap = new HashMap();
        sInnerMacro = sMacro.substring(sMacro.indexOf("(") + 1);
        sInnerMacro = sInnerMacro.substring(0, sInnerMacro.lastIndexOf(")"));
        for (String mapping : sInnerMacro.split(",")) {
            String[] mappingArr = mapping.trim().split("=>");
            
            // Add attribute by target name with value of the dataObject to the attribute Hashmap
            if (mappingArr.length == 1) {
                // Only one name has been specified, use this name as the source and target
                System.out.println("Only one name has been specified, use this name as the source and target");
                attrMap.put(mappingArr[0].trim(), dataObject.Attributes.GetValueAsString(mappingArr[0].trim()));
            } else {
                if (dataObject.Attributes.ContainsAttribute(mappingArr[0].trim())) {
                    // Attribute exists in the source
                    System.out.println("Attribute exists in the source");
                    attrMap.put(mappingArr[1].trim(), dataObject.Attributes.GetValueAsString(mappingArr[0].trim()));
                } else {
                    // Add attribute with empty string
                    System.out.println("Add attribute with empty string");
                    attrMap.put(mappingArr[1].trim(), "");
                }
            }
        }
        
        // Convert Hashmap to Matrix Attribute List
        _Result = mxAttr.createAttributeList(attrMap);
        
        // Check for date-values to reparse
        for (int iAttr = 0; iAttr < _Result.size(); iAttr ++){
            attr = ((matrix.db.Attribute)_Result.get(iAttr));
            
            // Get attribute data type
            AttributeType attrType;
            attrType = new AttributeType(attr.getName());
            
            // Check data type
            if (attrType.getDataType(context).equalsIgnoreCase("timestamp")) {
                if ("".equalsIgnoreCase(attr.getValue()) || attr.getValue() == null) {
                    // No value set, just leave current value and let application decide what to do
                } else {
                    // Date format, parse date-value
                    formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                    // Update value in attribute list
                    attr.setValue(DateFormatUtil.formatDate(context, formatter.parse(attr.getValue())));
                    _Result.setElementAt(attr, iAttr);
                }
            } else {
                // No date value, do not parse
            }
        }
                
        // Finished
        return _Result;
    }

    /**
     * Creates data object holding status information and adds it to the return package (if not null)
     * @param returnPackage Package that holds all failed objects which is returned to the invoking web service client
     * @param resultCode Process result
     * @param resultMessage Message to the client retrieving the return package
     * @param resultText Detailed information to the client retrieving the return package
     */
    public static void SetObjectResult(String objectId, DataPackage returnPackage, String resultCode, String resultMessage, String resultText) {
        ObjectAttribute __Attr;
        DataObject infoObject = new DataObject();

        infoObject.ObjectId = objectId;
        
        // Create error attributes
        __Attr = new ObjectAttribute();
        __Attr.Name = "ResultCode";
        __Attr.Value = resultCode;
        infoObject.Attributes.ObjectAttribute.add(__Attr);

        // Try to prevent empty error message (may occur when re-throwing exceptions
        __Attr = new ObjectAttribute();
        __Attr.Name = "ErrorMessage";
        if (resultMessage == null || "".equalsIgnoreCase(resultMessage) || " ".equalsIgnoreCase(resultMessage)) {
            // Empty, check for 'caused by' in the exception text
            if (resultText.toLowerCase().contains("caused by")) {
                __Attr.Value = resultText.substring(resultText.toLowerCase().lastIndexOf("caused by"));
            } else {
                // No caused by in the exception text, use first 200 characters of the exception
                if (resultText.length() > 200) {
                    __Attr.Value = resultText.substring(0, 199) + " ...... (more)";
                } else {
                    __Attr.Value = resultText;
                }
            }
        } else {
            // Not empty, use the exception message
            __Attr.Value = resultMessage;
        }
        infoObject.Attributes.ObjectAttribute.add(__Attr);

        __Attr = new ObjectAttribute();
        __Attr.Name = "ErrorText";
        __Attr.Value = resultText;
        infoObject.Attributes.ObjectAttribute.add(__Attr);

        // Check if data object is needed to be added to the return package
        if (returnPackage != null) {
            // Add failed data object to return package
            returnPackage.DataObjects.ObjectCollection.DataObject.add(infoObject);
        }
    }

    /**
     * Defines an error attribute for the specified data object (if not null)
     * @param returnPackage Package that holds all failed objects which is returned to the invoking web service client
     * @param errorText Error message
     * @param processLog LinkedList of string, containing all steps from the process (can be null)
     * @param PackageId Package identifier
     * @param objectIndex Object within the data package that generates the error
     * @throws IOException When writing to the log writer doesn't succeed
     */
    public static void setObjectProcessedError(DataPackage returnPackage, DataObject dataObjecta, String errorText, String errorMessage, LinkedList<String> processLog, String PackageId, int objectIndex) throws IOException, Exception {
        SetObjectResult(dataObjecta.ObjectId, returnPackage, "Error", errorMessage, errorText);
    }
            
    /**
     * Defines an error attribute for the specified data object (if not null)
     * @param returnPackage Package that holds all failed objects which is returned to the invoking web service client
     * @param dataObject Data object that is going to hold the error attribute(s)
     * @param errorText Error message
     * @throws IOException When writing to the log writer doesn't succeed
     */
    public static void setObjectProcessedWarning(DataPackage returnPackage, DataObject dataObject, String errorText, String errorMessage) throws IOException, Exception {
        if ("debug".equalsIgnoreCase(getLogSeverity()) || "information".equalsIgnoreCase(getLogSeverity())) {
            SetObjectResult(dataObject.ObjectId, returnPackage, "Warning", errorMessage, errorText);
        }
    }
            
    /**
     * Defines an error attribute for the specified data object (if not null)
     * @param returnPackage Package that holds all failed objects which is returned to the invoking web service client
     * @param dataObject Data object that is going to hold the error attribute(s)
     * @param processLog LinkedList of string, containing all steps from the process (can be null)
     * @throws IOException When writing to the log writer doesn't succeed
     */
    public static void setObjectProcessedSuccesfully(DataPackage returnPackage, DataObject dataObject, LinkedList<String> processLog) throws IOException, Exception {
        if ("debug".equalsIgnoreCase(getLogSeverity())) {
            SetObjectResult(dataObject.ObjectId, returnPackage, "Success", "", "");
        }
    }
    
    /**
     * Gets the data model mapping as stored within ENOVIA
     * @param context Connected Matrix context
     * @param configurationName Name of the page object that holds the configuration
     * @return Parsed DataModelMapping object
     * @throws Exception 
     */
    public static Mapping getMapping(Context context, String configurationName) throws Exception {
        Page configurationObject;
        Mapping mapping;
        
        // Get the page object, and check if it exists
        configurationObject = new Page(configurationName);
        if (configurationObject.exists(context)) {
            configurationObject.open(context);
            mapping = Mapping.parse(configurationObject.getContents(context));
            configurationObject.close(context);
        } else {
            // Does not exists
            throw new Exception("Unable to get the configuration by name: " + configurationName);
        }
        
        // Finished
        return mapping;
            
    }
    
    /**
     * Internal helper method that actually logging lines to a file
     * @param line
     * @throws Exception
     */
    private static void DoLogToFile(String line) throws Exception {
        File logFile = new File(getLogFile());
        if(!logFile.exists()) {
            logFile.createNewFile();
        } 
        FileWriter writer = new FileWriter(logFile, true);
        writer.append(line + System.getProperty("line.separator"));
        writer.close();
    }
    
    /**
     * Log to files whether the severity allows to do so..
     * @param line 
     * @param severity
     * @throws Exception 
     */
    public static void Log(String line, eLogSeverity severity) throws Exception {
        Boolean doLog;
        
        // Check log severity
        String configuredLogSeverity = getLogSeverity();
        doLog = false;
        switch (severity) {
            case error:
                // Check configured severity
                if ("none".equalsIgnoreCase(configuredLogSeverity)) {
                    doLog = false;
                } else {
                    // Always log error messages
                    doLog = true;
                }
                break;
            case information:
                // Check configured severity
                if ("debug".equalsIgnoreCase(configuredLogSeverity) || "information".equalsIgnoreCase(configuredLogSeverity)) {
                    doLog = true;
                }
                break;
            case debug:
                // Check configured severity
                if ("debug".equalsIgnoreCase(configuredLogSeverity)) {
                    doLog = true;
                }
                break;
        }
        
        // Check to log
        if (doLog) {
        	DoLogToFile(line);
        }
    }

    
    /**
     * Writes a line to the log file
     * @param line
     * @throws Exception 
     */
    private static void Log(String line) throws Exception {
    	Log(line, eLogSeverity.error);
    }
}