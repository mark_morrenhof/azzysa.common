package infostrait.azzysa.ev6.commonobjects;

import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Root class containing the entire package including data objects and mapping
 * @author m.morrenhof
 */
@XmlRootElement(name="DataPackage")
public class DataPackage {
    
    /**
     * Package identifier
     */
    public String PackageId;

    /**
     * Type mapping
     * @see DataModelMapping
     */
    @XmlElement(required = true, name="Mapping")
    public Mapping Mapping;
    
    @XmlElement(required = true, name="MappingReference")
    public String MappingReference;
        
    /**
     * Data objects
     * @see DataObjects
     */
    public DataObjects DataObjects;
    
    /**
     * Constructs a new empty data package
     */
    public DataPackage() {
        this.Mapping = new Mapping();
        this.DataObjects = new DataObjects();
    }
    
    /**
     * Reads as string serialized data package into in-memory data package object
     * @param packageString
     * @return  De-serialized package
     */
    public static DataPackage parse(String packageString) throws JAXBException {
        DataPackage _Package;
        JAXBContext _Ctx;            
        Unmarshaller _Un;
        StringReader _Rdr;
                                
        // Check paramter
        _Package = new DataPackage();
        if ("".equalsIgnoreCase(packageString)) {
            // Empty package string, return new data package
        } else {
            // Translate package string into in-memory package object
            
            // Use JAXB implementation to de-serialize the XML string
            _Ctx = JAXBContext.newInstance(DataPackage.class);
            _Un = _Ctx.createUnmarshaller();
            _Rdr = new StringReader(packageString);
            
            // De-serialize
            _Package = (DataPackage)_Un.unmarshal(_Rdr);
            _Rdr.close();
        }

        // Finished
        return _Package;
    }
    
    /**
     * Serializes this package as a XML string
     * @return
     * @throws JAXBException 
     */
    public String packageAsString() throws JAXBException {
        JAXBContext __Ctx;
        Marshaller __M;
        StringWriter __W;
        
        // Initialize serializer
        __Ctx = JAXBContext.newInstance(DataPackage.class);
        __M = __Ctx.createMarshaller();
        __M.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
        __W = new StringWriter();
        
        // Serialize
        __M.marshal(this, __W);
        
        // Return results
        return __W.toString();       
    }
}
