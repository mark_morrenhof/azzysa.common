package infostrait.azzysa.ev6.commonobjects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Collection of data objects
 * @author m.morrenhof
 */
public class ObjectCollection {
    
    /**
     * List of data objects
     * @see DataObject
     */
    public List<DataObject> DataObject;
    
    /**
     * Get all TypeInstructions sorted by the sequence attribute
     * @param attributeToCompare
     * @return List of type instructions sorted by their sequence
     * @see TypeInstruction
     */
    public List<DataObject> GetObjectsSorted(String attributeToCompare) {
        Collections.sort(DataObject, new DataObjectComparator(attributeToCompare));
        return this.DataObject;
    }
    
    /**
     * Get all TypeInstructions sorted by the date sequence attribute
     * @param attributeToCompare
     * @return List of type instructions sorted by their date sequence
     * @see TypeInstruction
     */
    public List<DataObject> GetObjectsSortedByDate(String revisionIdAttributeName, String creationDateAttributeName) {
        List<DataObject> revisionGroupSortedCollection;
        String previousRevisionGroup;
        List<DataObject> returnValue;
        
        // Sort the collection to group all revision groups
        returnValue = new LinkedList<DataObject>();
        revisionGroupSortedCollection = GetObjectsSorted(revisionIdAttributeName);
        String processedRevisionGroups = "";
                
        // Iterate through all objects sorted by revision group
        previousRevisionGroup = "";
        for(DataObject dataObject : revisionGroupSortedCollection) {
        	// Get revision group identifier
        	String revisionGroup = dataObject.Attributes.GetValueAsStringWithoutNullOrException(revisionIdAttributeName);
        	
        	// Get next revision group since in the next while loop we get all iterations from each revision group
        	if (!previousRevisionGroup.equalsIgnoreCase(revisionGroup) && (processedRevisionGroups.indexOf(revisionGroup + ";") < 0)) {
                // Store group to be processed
        		previousRevisionGroup = revisionGroup;
        		processedRevisionGroups += revisionGroup + ";";
        		
        		// Get all revisions from this group
        		List<DataObject> revisions = GetAllRevisionsById(revisionGroupSortedCollection, revisionIdAttributeName, revisionGroup);
        		
        		// Sort this collection on creation date
                Collections.sort(revisions, new DataObjectCreationDateComparator(creationDateAttributeName));
        		
        		// Add all objects to the complete collection
                returnValue.addAll(revisions);
        	}
        }
        
        return returnValue;
    }
    
    /**
     * Sorts the internal object collection according to the SmarTeam revision sequence
     * @param RevisionAttributeName
     * @param ParentRevisionAttributeName
     * @return List of type instructions sorted by their revision
     * @throws Exception 
     */
    public List<DataObject> GetObjectsSmarTeamRevisionSorted(String revisionIdAttributeName, String RevisionAttributeName, String ParentRevisionAttributeName) 
    		throws Exception {
        DataObject requestedRevision;
        List<DataObject> revisionGroupSortedCollection;
        List<DataObject> sortedCollection = new ArrayList<DataObject>();
        String previousRevisionGroup;
        
        // Sort the collection to group all revision groups
        revisionGroupSortedCollection = GetObjectsSorted(revisionIdAttributeName);
        String processedRevisionGroups = "";
                
        // Iterate through all objects sorted by revision group
        previousRevisionGroup = "";
        for(DataObject dataObject : revisionGroupSortedCollection) {
        	// Get revision group identifier
        	String revisionGroup = dataObject.Attributes.GetValueAsStringWithoutNullOrException(revisionIdAttributeName);
        	
        	// Get next revision group since in the next while loop we get all iterations from each revision group
        	if (!previousRevisionGroup.equalsIgnoreCase(revisionGroup) && (processedRevisionGroups.indexOf(revisionGroup + ";") < 0)) {
                // Take root / first revision from this group
        		previousRevisionGroup = revisionGroup;
        		processedRevisionGroups += revisionGroup + ";";
        		
        		// Check for null or empty root
                requestedRevision = GetRevision(revisionGroupSortedCollection, RevisionAttributeName, revisionIdAttributeName, revisionGroup, ParentRevisionAttributeName, null, true, true);
                if (requestedRevision == null) {
                	// Check for non null or empty root
                	requestedRevision = GetRevision(revisionGroupSortedCollection, RevisionAttributeName, revisionIdAttributeName, revisionGroup, ParentRevisionAttributeName, null, true, false);
                }
                        
                // Check if the first revision has been found
                if (requestedRevision == null) { throw new Exception("Invalid number of root revisions (should be 1, actual is 0)");}
                
                if(sortedCollection.size() > 50000)
                	throw new Exception("Possible infinite loop, check revision structure");
                sortedCollection.add(requestedRevision);
                
                // Iterate through collection until all revisions have been found
                while (requestedRevision != null) {
                    // Get next revision
                    requestedRevision = GetRevision(revisionGroupSortedCollection, RevisionAttributeName, revisionIdAttributeName, revisionGroup, ParentRevisionAttributeName, 
                    		requestedRevision.Attributes.GetValueAsStringWithoutNullOrException(RevisionAttributeName), false, false);
                    
                    // Check if revision has been found
                    if (requestedRevision != null) {
                    	if(sortedCollection.size() > 50000)
                        	throw new Exception("Possible infinite loop, check revision structure");
                        sortedCollection.add(requestedRevision);
                    }
                }
        	}
        }
        
        // Check if the number of sorted DataObjects is equal to the number of input objects
        if (sortedCollection.size() != DataObject.size()) {
            // Number of sorted revision objects is not according the number of input objects
            throw new Exception("Number of sorted revision objects (" + sortedCollection.size() + ") is not according the number of input objects(" + 
                    DataObject.size() + ")");
        } else {
            // Number of objects are equal, set the result collection to the package
            DataObject = sortedCollection;
        }
        
        // Finished, return sorted collection
        return this.DataObject;
    }
    
    /**
     * Helper method for the SmarTeamCreationDateSort function
     * @param RevisionAttributeName
     * @return 
     */
    private List<DataObject> GetAllRevisionsById(List<DataObject> collection, String revisionIdAttributeName, String revisionId) {
    	List<DataObject> returnValue = new LinkedList<DataObject>();
        for (DataObject obj : collection){
        	if (obj.Attributes.GetValueAsStringWithoutNullOrException(revisionIdAttributeName).equalsIgnoreCase(revisionId)) {
        		// Requested revision group, add to return collection
        		returnValue.add(obj);
        	}
        }
        
        // Not found
        return returnValue;
    }
    
    /**
     * Helper method for the SmarTeamRevisionSort function
     * @param RevisionAttributeName
     * @param ParentRevisionAttributeName
     * @param parentRevision
     * @return 
     */
    private DataObject GetRevision(List<DataObject> collection, String RevisionAttributeName, String revisionIdAttributeName, String revisionId, String ParentRevisionAttributeName, 
    		String parentRevision, boolean isRoot, boolean RootNullOrEmtpyRevision) {
        for (DataObject obj : collection){
        	if (obj.Attributes.GetValueAsStringWithoutNullOrException(revisionIdAttributeName).equalsIgnoreCase(revisionId)) {
              if ((isRoot && ("".equalsIgnoreCase(obj.Attributes.GetValueAsStringWithoutNullOrException(ParentRevisionAttributeName)) ||
                      " ".equalsIgnoreCase(obj.Attributes.GetValueAsStringWithoutNullOrException(ParentRevisionAttributeName)) ||
                      obj.Attributes.GetValueAsStringWithoutNullOrException(ParentRevisionAttributeName) == null))
                      || obj.Attributes.GetValueAsStringWithoutNullOrException(ParentRevisionAttributeName).equalsIgnoreCase(parentRevision)) {

            	  // Check whether to get a root revision and if the root revision should be null
            	  if (isRoot && RootNullOrEmtpyRevision && ("".equalsIgnoreCase(obj.Attributes.GetValueAsStringWithoutNullOrException(RevisionAttributeName)) ||
                          " ".equalsIgnoreCase(obj.Attributes.GetValueAsStringWithoutNullOrException(RevisionAttributeName)) ||
                          obj.Attributes.GetValueAsStringWithoutNullOrException(RevisionAttributeName) == null)) {
                	  // Requested revision (empty root is requested, and root is empty)
                      return obj;
            	  } else if (isRoot && !RootNullOrEmtpyRevision && !("".equalsIgnoreCase(obj.Attributes.GetValueAsStringWithoutNullOrException(RevisionAttributeName)) ||
                          " ".equalsIgnoreCase(obj.Attributes.GetValueAsStringWithoutNullOrException(RevisionAttributeName)) ||
                          obj.Attributes.GetValueAsStringWithoutNullOrException(RevisionAttributeName) == null)) {
                	  // Requested revision (no empty root is requested, and root is not empty)
                      return obj;
            	  } else if (!isRoot && obj.Attributes.GetValueAsStringWithoutNullOrException(ParentRevisionAttributeName).equalsIgnoreCase(parentRevision) && 
            			  !("".equalsIgnoreCase(obj.Attributes.GetValueAsStringWithoutNullOrException(RevisionAttributeName)) ||
                          " ".equalsIgnoreCase(obj.Attributes.GetValueAsStringWithoutNullOrException(RevisionAttributeName)) ||
                          obj.Attributes.GetValueAsStringWithoutNullOrException(RevisionAttributeName) == null)) {
                	  // Requested revision (no root revision requested, and parent revision matches requested parent, since no root revision is requested the current
            		  // revision value cannot be null (and therefore should not be null)
                      return obj;
            	  }
              }
        	}
        }
        
        // Not found
        return null;
    }
    
    /**
     * Constructs a new empty object collection
     */
    public ObjectCollection() {
        this.DataObject = new ArrayList();
    }
    
}