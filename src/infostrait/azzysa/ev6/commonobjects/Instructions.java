package infostrait.azzysa.ev6.commonobjects;

import java.util.Collections;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 * Holds a collection of type instructions
 * @author m.morrenhof
 */
public class Instructions {
    
    /**
     * List of type instructions
     * @see TypeInstruction
     */
    @XmlElement(required = true, name="TypeInstruction")
    public List<TypeInstruction> TypeInstruction;
    
    /**
     * Get all TypeInstructions sorted by the sequence attribute
     * @return List of type instructions sorted by their sequence
     * @see TypeInstruction
     */
    public List<TypeInstruction> GetInstructionsSorted() {
        Collections.sort(TypeInstruction, new InstructionComparator());
        return this.TypeInstruction;
    }
    
    /**
     * Constructs a new empty type instruction collection
     */
    public Instructions() {
        this.TypeInstruction = new java.util.ArrayList();
    }
}