package infostrait.azzysa.ev6.commonobjects;

import java.io.IOException;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author m.morrenhof
 */
public class EntityMapping {
    
    @XmlElement(required = true, name="EntityExpression")
    public String EntityExpression;
    
    @XmlElement(required = true, name="InvocationCollection")
    public TypeInvocationCollection invocationCollection;
    
    public EntityMapping() {
        this.invocationCollection = new TypeInvocationCollection();
        this.EntityExpression = "";
    }
    
    /**
     * Check whether the current entity mapping is valid for the specified data object
     * @param dataObject Data object to check
     * @return 
     * @throws java.io.IOException 
     */
    public Boolean IsValid(DataObject dataObject) throws RuntimeException, IOException, Exception {
        if (EntityExpression == null || EntityExpression.isEmpty() || dataObject == null) {
            // No expression to check
            return false;
        } else {
            // Check the expression
            EntityExpression = EntityExpression.trim();
            
            if (EntityExpression.toLowerCase().startsWith("attribute[")) {
               // Attribute expression
                String attributeName = EntityExpression.substring(EntityExpression.indexOf("[") + 1, EntityExpression.indexOf("]", 
                        EntityExpression.indexOf("[")));
                
                // Check if the attribute name is within the dataObject attribute collection
                if (dataObject.Attributes.ContainsAttribute(attributeName)) {
                    // Check the remaining part of the expression
                    String expression = EntityExpression.substring(EntityExpression.indexOf(attributeName) + attributeName.length());
                    expression = expression.substring(expression.indexOf("]") + 1).trim();
                    
                    // Check the remaining expression
                    if (expression.startsWith("==")) {
                        // Check attribute value
                        return (dataObject.Attributes.GetValueAsString(attributeName).equalsIgnoreCase(expression.substring(expression.indexOf("==") + 2).trim()));
                    } else if (expression.toLowerCase().startsWith(".hasvalue")) {
                        // Check if the attribute has a value
                        return !dataObject.Attributes.GetValueAsString(attributeName).isEmpty();
                    } else {
                        // Unknown attribute expression
                        throw new RuntimeException(EntityExpression + " => " + expression);
                    }
                    
                } else {
                    // Attribute is not present at this data object, this entity mapping is therefore not valid for the specifed data object
                    return false;
                }
                
            } else {
                // Unknown expression
                throw new RuntimeException(EntityExpression);
            }
        }
    }
}