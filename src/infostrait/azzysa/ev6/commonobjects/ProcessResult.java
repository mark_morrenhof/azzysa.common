package infostrait.azzysa.ev6.commonobjects;

import java.util.LinkedList;

/**
 * Holds the process results of a data object
 * @author m.morrenhof
 */
public class ProcessResult {
    
    public ProcessResult() {
        this.ProcessLog = new LinkedList<String>();
    }
    
    public enum eResult {
        Success,
        Warning,
        Error
    }
    
    public eResult result;
    
    public String exceptionText;
    
    public String exceptionMessage;
    
    public LinkedList<String> ProcessLog;
    
}
