/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package infostrait.azzysa.ev6.commonobjects;

/**
 * Used by utility class when querying for SmarTeam Object Id's
 * Is thrown when requested Object id is less than or equal to zero
 * @author infostrait
 */
public class ObjectIdentifierInvalidException extends Exception {
    
    public ObjectIdentifierInvalidException() {
        super();
    }
    
    public ObjectIdentifierInvalidException(String message) {
        super(message);
    }
    
    public ObjectIdentifierInvalidException(Throwable cause) {
        super(cause);
    }
    
    public ObjectIdentifierInvalidException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
