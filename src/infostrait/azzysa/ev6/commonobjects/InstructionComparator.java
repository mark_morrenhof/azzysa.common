package infostrait.azzysa.ev6.commonobjects;

import java.util.Comparator;

/**
 * Internal class, used by the DIADataService to order type instructions based on sequence
 * @author m.morrenhof
 */
public class InstructionComparator implements Comparator<TypeInstruction> {

    /**
     * Internal use only, compare type instructions
     * @param o1
     * @param o2
     * @return 
     */
    @Override
    public int compare(TypeInstruction o1, TypeInstruction o2) {
        if (o1.Sequence == o2.Sequence) {
            return 0;
        } else if (o1.Sequence > o2.Sequence) {
            return 1;            
        } else {
            return -1;
        }
    }
    
}
