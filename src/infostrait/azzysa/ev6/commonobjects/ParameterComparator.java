package infostrait.azzysa.ev6.commonobjects;

import java.util.Comparator;

/**
 * Internal use only, used for sorting purposes when invoking a functionResult
 * @author m.morrenhof
 */
public class ParameterComparator implements Comparator<Parameter> {

    /**
     * Compares two parameter instructions
     * @param o1
     * @param o2
     * @return 
     */
    @Override
    public int compare(Parameter o1, Parameter o2) {
        if (o1.Sequence == o2.Sequence) {
            return 0;            
        } else if (o1.Sequence > o2.Sequence) {
            return 1;
        } else {
            return -1;
        }
    }
    
}