package infostrait.azzysa.ev6.commonobjects;

import java.util.HashMap;
import java.util.List;

/**
 * Collection of attributes with their values
 * @author m.morrenhof
 */
public class Attributes {
    
    /**
     * List of attributes with their values
     * @see ObjectAttribute
     */
    public List<ObjectAttribute> ObjectAttribute;
    
    private HashMap<String, ObjectAttribute> __Indexed;
    
    private HashMap<String, ObjectAttribute> GetIndexed() {
         // Check whether the indexed collection has been initialized...
        if (__Indexed == null) {
            // Not yet initizlied, initialize now
            __Indexed = new HashMap<String, ObjectAttribute>();
            for (ObjectAttribute attribute : ObjectAttribute) {
                __Indexed.put(attribute.Name, attribute);
            }            
        }
        
        return __Indexed;
    }
    
    /**
     * Check whether this data object containing this attribute collection has an attribute with the specified name
     * @param name
     * @return Boolean value indicating whether the attribute exists within the collection
     */
    public boolean ContainsAttribute(String name) {
        return GetIndexed().containsKey(name);
    }
    
    /**
     * Return attribute value by name
     * @param name Name of the attribute to retrieve its value
     * @return The value of the attribute by it's name
     * @throws IndexOutOfBoundsException When attribute does not exists within the collection
     */
    public String GetValueAsString (String name) {

        // Check if the attribute exists
        if (GetIndexed().containsKey(name)) {
            // Does exists, return its value
            return GetIndexed().get(name).Value;
        } else {
            // Does not exists
            throw new IndexOutOfBoundsException(name);
        }
    }
    
    /**
     * Return attribute value by name
     * @param name Name of the attribute to retrieve its value
     * @return The value of the attribute by it's name, if the attribute does not exist or has a null value an empty string is returned
     */
    public String GetValueAsStringWithoutNullOrException (String name) {
        String value;
        
        // Check if the attribute exists
        if (GetIndexed().containsKey(name)) {
            // Does exists, return its value
            value = GetIndexed().get(name).Value;
            if (value == null) {value = "";}
        } else {
            // Does not exists
            value = "";
        }

        // Finished
        return value;
    }
    
    /**
     * Constructs a new empty object attribute collection
     */
    public Attributes() {
        this.ObjectAttribute = new java.util.ArrayList();
    }
    
}