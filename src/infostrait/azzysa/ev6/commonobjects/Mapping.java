package infostrait.azzysa.ev6.commonobjects;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author m.morrenhof
 */
@XmlRootElement(name="Mapping")
public class Mapping {
    
    @XmlElement(required = true, name="EntityMapping")
    public ArrayList<EntityMapping> EntityMapping;
    
    @XmlElement(required = false, name="SortAttribute")
    public String SortAttribute;
    
    public Mapping() {
        this.EntityMapping = new ArrayList<EntityMapping>();
    }
      
    /**
     * Serializes this Mapping as a XML string
     * @return
     * @throws JAXBException 
     */
    public String MappingAsString() throws JAXBException {
        JAXBContext __Ctx;
        Marshaller __M;
        StringWriter __W;
        
        // Initialize serializer
        __Ctx = JAXBContext.newInstance(Mapping.class);
        __M = __Ctx.createMarshaller();
        __M.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        __W = new StringWriter();
        
        // Serialize
        __M.marshal(this, __W);
        
        // Return results
        return __W.toString();       
    }
    
    /**
     * Reads as string serialized Mapping into an in-memory Mapping object
     * @param xmlString
     * @return  De-serialized package
     */
    public static Mapping parse(String xmlString) throws JAXBException {
        Mapping _mapping;
        JAXBContext _Ctx;            
        Unmarshaller _Un;
        StringReader _Rdr;
                                
        // Check paramter
        _mapping = new Mapping();
        if ("".equalsIgnoreCase(xmlString)) {
            // Empty package string, return new data package
        } else {
            // Translate package string into in-memory package object
            
            // Use JAXB implementation to de-serialize the XML string
            _Ctx = JAXBContext.newInstance(Mapping.class);
            _Un = _Ctx.createUnmarshaller();
            _Rdr = new StringReader(xmlString);
            
            // De-serialize
            _mapping = (Mapping)_Un.unmarshal(_Rdr);
            _Rdr.close();
        }

        // Finished
        return _mapping;
    }
    
}
