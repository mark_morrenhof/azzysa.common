package infostrait.azzysa.ev6.commonobjects;

/**
 * Log severity
 * @author m.morrenhof
 */
public enum eLogSeverity {
    debug,
    information,
    error
}