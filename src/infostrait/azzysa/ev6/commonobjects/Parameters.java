package infostrait.azzysa.ev6.commonobjects;

import java.util.Collections;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 * Collection of parameters used for method invocation
 * @author m.morrenhof
 */
public class Parameters {
    
    /**
     * Gets or sets an unsorted list of parameters
     * @see Parameter
     */
    @XmlElement(required = true, name="Parameter")
    public List<Parameter> Parameter;
    
    /**
     * Returns a sorted list of parameters
     * @return Sorted list of parameters
     * @see Parameter
     */
    public List<Parameter> GetParametersSorted() {
        Collections.sort(Parameter, new ParameterComparator());
        return this.Parameter;
    }
    
    /**
     * Constructs a new empty list of parameters
     */
    public Parameters() {
        this.Parameter = new java.util.ArrayList();
    }
}