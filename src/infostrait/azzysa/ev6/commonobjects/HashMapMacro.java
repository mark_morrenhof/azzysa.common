package infostrait.azzysa.ev6.commonobjects;

/**
 *
 * @author m.morrenhof
 */
public class HashMapMacro {
    
    private static String START_COMMAND = "$FillHashMap";
    private static String DATA_COMMAND = "$DATA_OBJECT";
    private static String ATTRIBUTE_COMMAND = "$ATTRIBUTE";
    
    /**
     * Decompiles the macro as string into an instruction set
     * @param sMacro Macro command as string
     * @param index Index within the sMacro parameter to continue processing the macro (recursive purposes only)
     * @param parent Parent instruction (in case of recursive
     * @return Instruction set
     * @throws Exception On any compile errors (invalid instructions)
     */
    public static InstructionBase Decompile(String sMacro, int index, InstructionBase parent) throws Exception {
        InstructionBase returnValue;
        InstructionBase innerCommand;
        String sSelection;
        int idxCommandSep;
                
        // Check for empty string
        if ("".equalsIgnoreCase(sMacro)) {
            // Emtpy instruction
            return new InstructionBase();
        } 
               
        // Continue with decompiling
        sSelection = "";
        returnValue = null;
        for (int idx = index; idx < sMacro.length(); idx++) {
            
            // Check whether the current character represents an opening or closing tag (for a new/current instruction)
            if ("{".equalsIgnoreCase(sMacro.substring(idx, idx + 1))) {                
                // Start (inner) command -> do nothing at this iteration                                        
            } else if ("}".equalsIgnoreCase(sMacro.substring(idx, idx + 1))) {
                // End (inner) command, check the current instruction
                
                if (returnValue instanceof HashMapFillInstruction) {
                    // Valid, recognized instruction
                    returnValue.EndIndex = idx;        
                    
                    // Return the command
                    return returnValue;
                            
                } else {
                    // Unknown/invalid instruction
                    throw new Exception("Unknown expression at index " + returnValue.StartIndex);
                }
                
            } else {
                // Append character to selection
                sSelection = sSelection + sMacro.charAt(idx);
                
                // Check current selection (if type of instruction is already defined in selection)
                if (sSelection.startsWith(START_COMMAND)) {
                    // Fill HashMap instruction
                    returnValue = new HashMapFillInstruction();
                    ((HashMapFillInstruction)returnValue).StartIndex = index;
                    
                    // Invoke this method recursively in order to process this HashMapFillInstruction
                    innerCommand = Decompile(sMacro, idx + 1, returnValue);
                    returnValue.instructions.add(innerCommand);

                    // Check for infinite loop
                    if (innerCommand.EndIndex <= idx) {
                        // Syntax error resulting in an infinite loop
                        throw new Exception("Syntax error resulting in an infinite loop at index: " + idx);
                    }
                    
                    // Increment current index with one to skip the next close character
                    idx = sMacro.indexOf(")", innerCommand.EndIndex);
                    
                    // Reset selection
                    sSelection = "";
                    
                    innerCommand = null;
                    
                } else if (sSelection.contains(DATA_COMMAND)) {
                    // Check whether current instruction is a HashMapFillInstruction
                    if (parent instanceof HashMapFillInstruction) {
                        
                        // Set Data Object parameter to true
                        ((HashMapFillInstruction)parent).SetDataObject = true;
                        
                        // Get the comma seperator for next command in order to check whether the comma character is set before the end-tag of the current instruction
                        // Since we are at the end of the $DATA_OBJECT parameter the first comma (separates the first and second parameter)
                        // will be skipped and the next comma separator is searched
                        idxCommandSep = sMacro.indexOf(",", sMacro.indexOf(",", idx) + 1);
                        
                        // Start processing the second parameter of the HashMapFillInstruction (the attributes)
                        if (idxCommandSep < sMacro.indexOf(")", idx) && idxCommandSep >=0 ) {
                            // Syntax error, 2nd parameter not set properly
                            throw new Exception("Syntax error, 2nd parameter not set properly at index: "
                                    + idx);
                        } else {
                            // Attributes definition, continue and reset selection
                            sSelection = "";
                        }
                        
                    } else if (sSelection.contains(ATTRIBUTE_COMMAND)) {
                        // Filter attribute expression of current selection 
                        innerCommand = new HashMapKeyValueInstruction();
                        innerCommand.StartIndex = idx;
                        
                        // Check Key-Value
                        if (sMacro.indexOf(",", idx) < sMacro.indexOf(")", idx)) {
                            // Syntax error, 2nd parameter not set properly
                            throw new Exception("Syntax error, attribute expression not properly defined at index: "
                                    + idx);
                        } else if (! sSelection.contains("=")) {
                            // No key value separator
                            throw new Exception("Syntax error, attribute expression not properly defined at index (no key-value seperator): "
                                    + idx);
                        } else {
                            // Get key/value expression            
                            innerCommand.Key = sSelection.substring(0, sSelection.indexOf("=")).trim();
                            innerCommand.Value = sMacro.substring(sMacro.indexOf("(", idx + 1), sMacro.indexOf(")", idx + 2));
                            innerCommand.EndIndex = sMacro.indexOf(")", idx + 2);
                             
                            // Add to parent command (check if not null)
                            if (parent != null) {
                                // Add to parent collection
                                parent.instructions.add(innerCommand);
                            } else {
                                // Invalid key-value instruction without parent instruction
                                throw new Exception("Invalid key-value instruction without parent instruction at index: " + 
                                        innerCommand.StartIndex);
                            }
                        }
                    } else if (", \"\")".equalsIgnoreCase(sSelection) && parent instanceof HashMapFillInstruction) {
                        // Closing tag of a HashMapFillInstruction instruction
                        // Set end index and clear selection
                        parent.EndIndex = idx;
                        sSelection = "";
                    } else {
                        // Unknown instruction combined with the Set Data Object instruction
                        throw new Exception("Unknown instruction combined with the Set Data Object instruction at index: "
                                + (idx - sSelection.length()));
                    }
                } else {
                    // Just keep appending characters to the selection
                }
            }
        }
        
        // Check if current selection is empty (should be - otherwise the expression is not properly closed)
        if ("".equalsIgnoreCase(sSelection)) {
            // Properly ended
            return returnValue;
        } else {
            // Not properly ended
            throw new Exception("Expression not properly ended: '....." + sSelection + ".....'");
        }
    }
    
}