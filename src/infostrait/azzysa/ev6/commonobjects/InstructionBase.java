package infostrait.azzysa.ev6.commonobjects;

import java.util.ArrayList;

/**
 * Internal class - serves as base class to support collections
 * @author m.morrenhof
 */
public class InstructionBase {

    /**
     * Creates new InstructionBase object
     */
    public InstructionBase() {   
        instructions = new ArrayList<InstructionBase>();
    }
    
    /**
     * Index within macro string that current instruction is defined
     */
    public int StartIndex;
    
    /**
     * Index within macro string that current instruction ends
     */
    public int EndIndex;
    
    /**
     * Gets or sets the name as stored within any parent HashMap collection
     */
    public String Key;
    
     /**
     * Gets or sets the value as stored within any parent HashMap collection
     */
    public String Value;
    
    /**
     * Collection of fill instruction
     */
    public ArrayList<InstructionBase> instructions;
    
}