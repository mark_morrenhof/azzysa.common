package infostrait.azzysa.ev6.commonobjects;

/**
 *
 * @author m.morrenhof
 */
public class HashMapFillInstruction extends InstructionBase {

    /**
     * Creates new HashMapFillInstruction object
     */
    public HashMapFillInstruction() {  
        super();
    }
    
    /**
     * Provide data object when filling the HashMap
     */
    public boolean SetDataObject;
 
}