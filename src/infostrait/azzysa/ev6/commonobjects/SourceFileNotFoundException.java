/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package infostrait.azzysa.ev6.commonobjects;

/**
 * Is thrown when the specified source file cannot be found
 * @author infostrait
 */
public class SourceFileNotFoundException extends Exception {
    
    private String __FileName;
    
    /**
     * Returns the report file that cannot be found
     * @return 
     */
    public String getFileName() {
        return __FileName;
    }
    
    public SourceFileNotFoundException() {
        super();
    }
    
    public SourceFileNotFoundException(String FileName) {
        super("Source file: " + FileName + "cannot be found!");
        __FileName = FileName;
    }
    
}