package infostrait.azzysa.ev6.commonobjects;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import matrix.db.Context;

/**
 *
 * @author m.morrenhof
 */
public class TypeInvocation {
    
    @XmlElement(required = true, name="TypeName")
    public String TypeName;
    
    /**
     * Invocation style: Static / Instance
     */
    @XmlElement(required = true, name="InvocationModifier")
    public String InvocationModifier;
    
    @XmlElement(required = true, name="ConstructorParameters")
    public Parameters constructorArgs;
    
    @XmlElement(required = true, name="Instructions")
    public Instructions instructions;
    
    public TypeInvocation() {
        this.TypeName = "";
        this.InvocationModifier = "Instance";
        this.instructions = new Instructions();
        this.constructorArgs = new Parameters();
    }

    /**
     * Function invokes the requested type as specified in typeInvocation and returns the result as an object
     * @param context Connected ENOVIA context
     * @param dataObject DataObject object holding all the source attributes
     * @param classLoader Class loader that is used to load the requested types by this function result instruction
     * @return The result of the type invocation
     * @throws Exception 
     */
     public ArrayList<Object> DoCollectInvocationResult(Context context, DataObject dataObject, ClassLoader classLoader) throws Exception {
        @SuppressWarnings("rawtypes")
		Class _EnoviaClass;
        Object _EnoviaObject;
        MethodInvocationSet _Method;
        MethodInvocationSet _Constructor;
        ArrayList<Object> returnValues;
                
        // Log for troubleshoot/debug purposes
        returnValues = new ArrayList<Object>();
        
        // Verify that there are instructions provided by this mapping
        if (TypeName == null || "".equalsIgnoreCase(TypeName)) {
            // No instructions
            throw new Exception("[DoCollectInvocationResult] : No type name (0)");
        }

        // Load the ENOVIA class to create this data object                
        _EnoviaClass = classLoader.loadClass(TypeName);

        // Check for constructor arguments
        if (this.constructorArgs != null & this.constructorArgs.Parameter.size() > 0) {
            // Use parameterized constructor
            // Get method invocation set for constructor
            _Constructor = MethodInvocationSet.getMethodInvocation(context, classLoader, dataObject, _EnoviaClass, 
                    constructorArgs, "__Constructor", true);

            // Instantiate object
            _EnoviaObject = _Constructor.Constructor.newInstance(_Constructor.InvokeParameters);

        } else {
            // Use parameterless constructor
            _EnoviaObject = _EnoviaClass.newInstance();
        }

        // Always add the instantiated ENOVIA object as the first object in the return collection
        returnValues.add(_EnoviaObject);

        // Iterate through all instructions in order to successfully create the new data object
        if (instructions != null) {
            for (TypeInstruction typeInstruction : instructions.GetInstructionsSorted()) {

                // Get method to invoke
                _Method = MethodInvocationSet.getMethodInvocation(context, classLoader, dataObject, _EnoviaClass, typeInstruction.Parameters, 
                        typeInstruction.MemberName, false);

                // Invoke this method
                if (_Method.Method.getReturnType() == void.class) {
                    // Do not use the return value void, just invoke the method
                    _Method.Method.invoke(_EnoviaObject, _Method.InvokeParameters);
                } else {
                    // Add the return value the result collection
                    returnValues.add(_Method.Method.invoke(_EnoviaObject, _Method.InvokeParameters));
                }

                // Finished
                _Method = null;
            }
        }
        
        // Finished
        _EnoviaClass = null;
        return returnValues;
    }
}