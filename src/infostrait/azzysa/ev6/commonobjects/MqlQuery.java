package infostrait.azzysa.ev6.commonobjects;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Object represents an MQL query
 *
 * @author t.v.oost
 */
@XmlRootElement
public class MqlQuery {

    public String Query;

    public MqlQuery() {
    }
}